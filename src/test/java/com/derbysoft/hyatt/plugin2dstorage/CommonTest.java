package com.derbysoft.hyatt.plugin2dstorage;

import com.derby.dswitch.ari.model.OccupancyRate;
import junit.framework.TestCase;
import org.joda.time.LocalDate;
import org.junit.Test;

import java.util.Comparator;
import java.util.Map;
import java.util.TreeMap;

public class CommonTest {

    @Test
    public void testTreeSort(){
        Map<Integer, String> map = new TreeMap<>(Comparator.comparingInt(o -> o));
        map.put(4, "d");
        map.put(1, "a");
        map.put(3, "c");
        map.put(2, "b");

        System.out.println(map.values());
        System.out.println("buildRawRestriction,LosRate".contains("LosRate"));
    }

    @Test
    public void testDate(){
        System.out.println(new LocalDate("2020-05-01").plusMonths(1));
    }

}