package com.derbysoft.hyatt.plugin2dstorage.exclude;

import com.derbysoft.schedulecenter.rpc.client.HttpScheduleCenterService;
import com.derbysoft.schedulecenter.rpc.protocol.CreateTasks;
import com.derbysoft.schedulecenter.rpc.protocol.CreateTasksResult;
import com.derbysoft.schedulecenter.rpc.protocol.Task;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.SetMultimap;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;

import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.*;

public class SyncPlugin2DstorageApp {

    String target = "HYATT.PLUGIN_ARI_TO_DSTORAGE.MONTH";
    HttpScheduleCenterService scheduleCenterService = new HttpScheduleCenterService("schedule-center-task-distributor.derbysoftapi.com/schedule-center-tasks");

    @Test
    public void app() throws Exception {
//        getHotelRatePlanFromWhiteList().forEach((k, v) -> {
//            System.out.println(k + "=" + v);
//        });
        ArrayList<String> hotelCodes = Lists.newArrayList();

        Map<String, String> hotelRatePlanFromWhiteList = getHotelRatePlanFromWhiteList();
        for (String hotelCode : hotelRatePlanFromWhiteList.keySet()) {
            String rateplnas = hotelRatePlanFromWhiteList.get(hotelCode);
            if (rateplnas.contains("META_RACK")
                    || rateplnas.contains("META_MYHIBK")
                    || rateplnas.contains("META_PROM")
                    || rateplnas.contains("META_U15O")) {
                hotelCodes.add(hotelCode);
            }
        }

        System.out.println(hotelCodes.size());
        ArrayList<String> list = Lists.newArrayList(
                "2020-07",
                "2020-08",
                "2020-09",
                "2020-10"
//                "2020-11",
//                "2020-12",
//                "2021-01",
//                "2021-02",
//                "2021-03",
//                "2021-04",
//                "2021-05",
//                "2021-06"
        );

        HashMultimap<String, String> updateLists = HashMultimap.create();
        Map<String, String> hotelRatePlans = new HashMap<>();

        for (String hotelCode : hotelCodes) {
            for (String s : list) {
                updateLists.put(hotelCode, s);
            }
//            hotelRatePlans.put(hotelCode, "META_U15O,META_RACK,META_MYHI,META_15OFF,META_PROM,META_LOV1");
            hotelRatePlans.put(hotelCode, "META_MYHIBK,META_RACK,META_U15O,META_PROM");
        }

        createTask(updateLists, hotelRatePlans);

    }


    private void createTask(SetMultimap<String, String> hotelMonths, Map<String, String> ratePlans) {
        for (String hotelCode : hotelMonths.keySet()) {
            Set<String> monTh = hotelMonths.get(hotelCode);
            for (String m : monTh) {
                List<Task> tasks = new ArrayList<>();
                Task t = new Task();
                t.setCreateTime(new Date().getTime());
                t.setId("Manual" + UUID.randomUUID().toString().replace("-", "").substring(6));
                Map<String, String> params = new HashMap<>();
                params.put("channelCode", "DPLATFORM");
                params.put("providerCode", "HYATT");
                params.put("hotelCode", hotelCode);
                params.put("maxLos", "30");
                params.put("crNumbers", "META");
                params.put("yearMonth", m);
                params.put("category", "buildRawRestriction");
                params.put("rateplans", ratePlans.get(hotelCode));

                t.setParametersMap(params);
                t.setPriority(5);
                tasks.add(t);

                CreateTasks createTasks = new CreateTasks();
                createTasks.setTarget(target);
                createTasks.setTasks(tasks);
                CreateTasksResult result = scheduleCenterService.exec(createTasks);
                System.out.println(result.isSuccess());
            }
        }
    }

    private Map<String, String> getHotelRatePlanFromWhiteList() throws Exception {
        InputStream resourceAsStream = new ClassPathResource("st_white_list.text").getInputStream();
        List<String> lines = IOUtils.readLines(resourceAsStream, Charset.defaultCharset());

        HashMultimap<String, String> hotelRps = HashMultimap.create();
        for (String line : lines) {
            if (StringUtils.isBlank(line)) {
                continue;
            }
            String[] split = line.split(",");
            String hotelCode = split[0];
            String ratePlan = split[1];
            if (!ratePlan.startsWith("META_")) {
                ratePlan = "META_" + ratePlan;
            }

            hotelRps.put(hotelCode.trim(), ratePlan.trim());
        }

        Map<String, String> result = new HashMap<>();
        for (String hotelCode : hotelRps.keySet()) {
            Set<String> rateplans = hotelRps.get(hotelCode);
            result.put(hotelCode, String.join(",", rateplans));
        }
        return result;
    }

}
