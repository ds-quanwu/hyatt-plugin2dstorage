package com.derbysoft.hyatt.plugin2dstorage.exclude;

import com.derby.dswitch.ari.model.OccupancyRate;
import com.derby.dswitch.ari.soap.Envelope;
import com.derby.dswitch.ari.wrap.GetAvailabilityChangeResponse;
import com.derby.nuke.common.adapter.ari.ARIStreamSerializer;
import com.derbysoft.common.util.xml.XStreamUtils;
import com.derbysoft.hyatt.plugin2dstorage.domain.DateRoomCategory;
import com.derbysoft.hyatt.plugin2dstorage.domain.LosAriData;
import com.derbysoft.hyatt.plugin2dstorage.translator.LosRateTranslator;
import com.derbysoft.storage.remote.dto.SaveLosRateRequest;
import com.derbysoft.storage.remote.dto.SaveRestrictionRequest;
import com.google.common.collect.Sets;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.HashSet;
import java.util.List;
import java.util.Map;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:ccs.xml", "classpath:core.xml"})
public class LosRateTranslatorTest {
    @Autowired
    LosRateTranslator losRateTranslator;

    @Test
    public void test01() throws Exception {
        ClassPathResource classPathResource = new ClassPathResource("nuke.los3.xml");
//
        Envelope unmarshal = (Envelope) ARIStreamSerializer.getInstance().unmarshal(classPathResource.getInputStream(), "UTF-8");

        System.out.println(unmarshal.getBody().getMessage().getClass().getName());

        GetAvailabilityChangeResponse response = (GetAvailabilityChangeResponse) unmarshal.getBody().getMessage();
        System.out.println(response.getAvailabilityUpdate().getHotelCode());


        //LosRateTranslator losRateTranslator = new LosRateTranslator();
        LosAriData losAriData = losRateTranslator.toAriData("xx", "xxx", 30, Sets.newHashSet("META"), response, () -> new HashSet<>());

        Map<DateRoomCategory, Map<Integer, List<OccupancyRate>>> rates = losAriData.getLosRates();

        SaveLosRateRequest saveLosRateRequest = losRateTranslator.toLosRateRequest(losAriData);
        SaveRestrictionRequest restrictionRequest = losRateTranslator.toRestrictionRequest(losAriData);
        SaveRestrictionRequest restrictionRequest1 = losRateTranslator.toRestrictionStatusRequest(losAriData);
        System.out.println(XStreamUtils.toXML(restrictionRequest));
        System.out.println(XStreamUtils.toXML(restrictionRequest1));


    }

}