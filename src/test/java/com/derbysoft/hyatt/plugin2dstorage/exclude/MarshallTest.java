package com.derbysoft.hyatt.plugin2dstorage.exclude;

import com.derby.dswitch.ari.soap.Envelope;
import com.derby.dswitch.ari.wrap.GetAvailabilityChangeResponse;
import com.derby.nuke.ari.model.Contract;
import com.derby.nuke.ari.model.DateSpan;
import com.derby.nuke.ari.model.RetrieveChangeCriteria;
import com.derby.nuke.ari.utils.JibxUtils;
import com.derby.nuke.ari.wrap.GetDailyChangeRequest;
import com.derby.nuke.common.adapter.ari.ARIStreamSerializer;
import com.derbysoft.common.util.xml.XStreamUtils;
import com.derbysoft.hyatt.plugin2dstorage.domain.LosAriData;
import com.derbysoft.hyatt.plugin2dstorage.translator.LosRateTranslator;
import com.derbysoft.storage.remote.dto.SaveLosRateRequest;
import com.google.common.collect.Sets;
import org.apache.commons.io.IOUtils;
import org.jibx.runtime.IMarshallingContext;
import org.jibx.runtime.IUnmarshallingContext;
import org.joda.time.LocalDate;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.HashSet;

public class MarshallTest {

    @Test
    public void test01() throws Exception {
        ClassPathResource classPathResource = new ClassPathResource("nuke.los2.xml");
//
//        Jaxb2Marshaller jaxb2Marshaller = new Jaxb2Marshaller();
//        jaxb2Marshaller.setContextPath("com.derbysoft.hyatt.plugin2dstorage.protocol");
//
//        Object unmarshal = jaxb2Marshaller.unmarshal(new StreamSource(classPathResource.getInputStream()));
//
//        System.out.println(unmarshal.getClass().getName());
        Envelope unmarshal = (Envelope) ARIStreamSerializer.getInstance().unmarshal(classPathResource.getInputStream(), "UTF-8");

        System.out.println(unmarshal.getBody().getMessage().getClass().getName());

        GetAvailabilityChangeResponse response = (GetAvailabilityChangeResponse) unmarshal.getBody().getMessage();
        System.out.println(response.getAvailabilityUpdate().getHotelCode());


        LosRateTranslator losRateTranslator = new LosRateTranslator();
        LosAriData losAriData = losRateTranslator.toAriData("DPLATFORM", "CNMXC", 30, Sets.newHashSet("META"), response, () -> new HashSet<>());

        SaveLosRateRequest saveLosRateRequest = losRateTranslator.toLosRateRequest(losAriData);

        String xml = XStreamUtils.toXML(saveLosRateRequest);

        FileOutputStream fileOutputStream = new FileOutputStream("/home/traitswu/derby-workspace/hyatt-plugin2dstorage/target/tmp.xml");
        IOUtils.write(xml, fileOutputStream, "UTF-8");
        IOUtils.closeQuietly(fileOutputStream);
//        System.out.println(xml);
        System.out.println(losAriData);

    }

    @Test
    public void test02() throws Exception {
        GetDailyChangeRequest dailyChangeRequest = new GetDailyChangeRequest();
        dailyChangeRequest.setContract(new Contract("CTRIP", "HYATT"));
        RetrieveChangeCriteria criteria = new RetrieveChangeCriteria();
        criteria.setHotelCode("LEXRL");
        criteria.setType("All");
        criteria.setDateSpan(new DateSpan(new LocalDate("2020-10-08"), new LocalDate("2020-10-08")));
        criteria.setRequestCategories(Sets.newHashSet(
                "LosInventory",
                "Inventory",
                "Restriction",
                "Rate"
        ));
        dailyChangeRequest.setRetrieveChangeCriteria(criteria);


//        ARIStreamSerializer.getInstance().marshal(dailyChangeRequest, System.out, "UTF-8");

        IMarshallingContext marshallingContext = JibxUtils.getIBindingFactory().createMarshallingContext();
        marshallingContext.marshalDocument(dailyChangeRequest, "UTF-8", null, System.out);
    }


}
