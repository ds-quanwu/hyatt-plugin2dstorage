package com.derbysoft.hyatt.plugin2dstorage.exclude;

import com.derby.dswitch.ari.model.Contract;
import com.derby.dswitch.ari.model.DateSpan;
import com.derby.dswitch.ari.model.RetrieveChangeCriteria;
import com.derby.dswitch.ari.wrap.GetAvailabilityChangeRequest;
import com.derby.dswitch.ari.wrap.GetAvailabilityChangeResponse;
import com.derby.nuke.ari.wrap.GetDailyChangeRequest;
import com.derby.nuke.ari.wrap.GetDailyChangeResponse;
import com.derby.nuke.common.adapter.ari.ARIStreamSerializer;
import com.derbysoft.common.util.xml.XStreamUtils;
import com.derbysoft.hyatt.plugin2dstorage.domain.LosAriData;
import com.derbysoft.hyatt.plugin2dstorage.translator.DailyRateTranslator;
import com.derbysoft.hyatt.plugin2dstorage.translator.LosRateTranslator;
import com.derbysoft.hyatt.plugin2dstorage.web.ApsUrlService;
import com.derbysoft.hyatt.plugin2dstorage.web.PluginWebService;
import com.derbysoft.storage.remote.dto.SaveLosRateRequest;
import com.derbysoft.storage.remote.dto.SaveRestrictionRequest;
import com.google.common.collect.Sets;
import org.joda.time.LocalDate;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.ByteArrayOutputStream;
import java.util.HashSet;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:ccs.xml", "classpath:core.xml"})
public class WebServiceTest {

    @Autowired
    LosRateTranslator losRateTranslator;

    @Test
    public void testGetFromPlugin() throws Exception {
        PluginWebService pluginWebService = new PluginWebService();
        pluginWebService.setApsUrlService(new ApsUrlService());

        GetAvailabilityChangeRequest request = new GetAvailabilityChangeRequest();
        request.setToken("BY-Plugin2dstroage");
        request.setContract(new Contract("HOTELBEDS", "HYATT"));
        RetrieveChangeCriteria criteria = new RetrieveChangeCriteria();
        criteria.setHotelCode("OGGAW");
        criteria.setType("All");
        criteria.setDateSpan(new DateSpan(new LocalDate("2020-11-06"), new LocalDate("2020-11-06")));
//        criteria.setRoomTypeCodes(Sets.newHashSet("1VST"));
        criteria.setRatePlanCodes(Sets.newHashSet("26741_WPRMX6"));
        criteria.setLosCandidates(Sets.newHashSet(7));
        request.setRetrieveChangeCriteria(criteria);

        GetAvailabilityChangeResponse response = pluginWebService.getLosRate(request);

        System.out.println(marshall(response));

        LosAriData losAriData = losRateTranslator.toAriData("HOTELBEDS", "OGGAW", 2, Sets.newHashSet("26741"), response, () -> new HashSet<>());

        SaveRestrictionRequest restrictionRequest = losRateTranslator.toRestrictionRequest(losAriData);
        SaveRestrictionRequest restrictionStatusRequest = losRateTranslator.toRestrictionStatusRequest(losAriData);
        SaveLosRateRequest saveLosRateRequest = losRateTranslator.toLosRateRequest(losAriData);

        System.out.println(XStreamUtils.toXML(saveLosRateRequest));
    }

    @Test
    public void testGetDaily() throws Exception {
        GetDailyChangeRequest dailyChangeRequest = new GetDailyChangeRequest();
        dailyChangeRequest.setContract(new com.derby.nuke.ari.model.Contract("CTRIP", "HYATT"));
        com.derby.nuke.ari.model.RetrieveChangeCriteria criteria = new com.derby.nuke.ari.model.RetrieveChangeCriteria();
        criteria.setHotelCode("LEXRL");
        criteria.setType("All");
        criteria.setDateSpan(new com.derby.nuke.ari.model.DateSpan(new LocalDate("2020-10-06"), new LocalDate("2020-10-08")));
        criteria.setRequestCategories(Sets.newHashSet(
                "LosInventory",
                "Inventory",
                "Restriction",
                "Rate"
        ));
        dailyChangeRequest.setRetrieveChangeCriteria(criteria);

        PluginWebService pluginWebService = new PluginWebService();
        pluginWebService.setApsUrlService(new ApsUrlService());

        GetDailyChangeResponse response = pluginWebService.getDailyRate(dailyChangeRequest);
        DailyRateTranslator dailyRateTranslator = new DailyRateTranslator();
        SaveRestrictionRequest saveRestrictionRequest = dailyRateTranslator.toDailyInventory("CTRIP", "LEXRL", Sets.newHashSet("07781", "07783"), response, () -> new HashSet<>());
        System.out.println(XStreamUtils.toXML(saveRestrictionRequest));


//        System.out.println(response);
//        JibxUtils.getIBindingFactory().createMarshallingContext().marshalDocument(response, "UTF-8", null, System.out);

    }

    @Test
    public void testGetPluginUrl() {
        ApsUrlService apsUrlService = new ApsUrlService();
        String url = apsUrlService.getUrl("HYATT", "MEITUAN");
        System.out.println(url);

    }

    private String marshall(Object obj) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream(1024 * 1024 * 5);
        ARIStreamSerializer.getInstance().marshal(obj, outputStream, "UTF-8");
        return outputStream.toString();
    }
}
