package com.derbysoft.hyatt.plugin2dstorage.exclude;

import com.derbysoft.schedulecenter.rpc.client.HttpScheduleCenterService;
import com.derbysoft.schedulecenter.rpc.protocol.CreateTasks;
import com.derbysoft.schedulecenter.rpc.protocol.CreateTasksResult;
import com.derbysoft.schedulecenter.rpc.protocol.Task;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.SetMultimap;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;

import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.*;

public class GetaRoomSyncApp {
    String target = "HYATT.PLUGIN_ARI_TO_DSTORAGE.MONTH";
    HttpScheduleCenterService scheduleCenterService = new HttpScheduleCenterService("schedule-center-task-distributor.derbysoftapi.com/schedule-center-tasks");


    @Test
    public void app() throws Exception {
        InputStream resourceAsStream = new ClassPathResource("getarrom.hotel.list").getInputStream();
        List<String> lines = IOUtils.readLines(resourceAsStream, Charset.defaultCharset());



        List<String> hotelCodes = new ArrayList<>();
        for (String line : lines) {
            if (StringUtils.isNotBlank(line)) {
                hotelCodes.add(line.trim());
            }
        }
        System.out.println(hotelCodes.size());


        ArrayList<String> list = Lists.newArrayList(
//                "2020-07",
//                "2020-08",
//                "2020-09",
//                "2020-10"
                "2020-11",
                "2020-12",
                "2021-01",
                "2021-02",
                "2021-03",
                "2021-04",
                "2021-05",
                "2021-06",
                "2021-07",
                "2021-08"
        );

        HashMultimap<String, String> updateLists = HashMultimap.create();
        Map<String, String> hotelRatePlans = new HashMap<>();

        for (String hotelCode : hotelCodes) {
            for (String s : list) {
                updateLists.put(hotelCode, s);
            }
        }

        createTask(updateLists, hotelRatePlans);

    }


    private void createTask(SetMultimap<String, String> hotelMonths, Map<String, String> ratePlans) {
        for (String hotelCode : hotelMonths.keySet()) {
            Set<String> monTh = hotelMonths.get(hotelCode);
            for (String m : monTh) {
                List<Task> tasks = new ArrayList<>();
                Task t = new Task();
                t.setCreateTime(new Date().getTime());
                t.setId("Manual" + UUID.randomUUID().toString().replace("-", "").substring(6));
                Map<String, String> params = new HashMap<>();
                params.put("channelCode", "GETAROOM");
                params.put("providerCode", "HYATT");
                params.put("hotelCode", hotelCode);
                params.put("maxLos", "7");
                params.put("crNumbers", "49584");
                params.put("yearMonth", m);
                params.put("category", "buildRawRestriction,LosRate");
                params.put("rateplans", ratePlans.get(hotelCode));

                t.setParametersMap(params);
                t.setPriority(5);
                tasks.add(t);

                CreateTasks createTasks = new CreateTasks();
                createTasks.setTarget(target);
                createTasks.setTasks(tasks);
                CreateTasksResult result = scheduleCenterService.exec(createTasks);
                System.out.println(result.isSuccess());
            }
        }
    }
}
