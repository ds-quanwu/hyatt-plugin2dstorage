package com.derbysoft.hyatt.plugin2dstorage.exclude;

import com.derbysoft.common.util.xml.XStreamUtils;
import com.derbysoft.hyatt.plugin2dstorage.dto.SimpleRQ;
import com.derbysoft.hyatt.plugin2dstorage.executor.DailyRateGetter;
import com.derbysoft.hyatt.plugin2dstorage.translator.LosRateTranslator;
import com.derbysoft.storage.remote.dto.SaveDailyRateRequest;
import com.google.common.collect.Sets;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:ccs.xml", "classpath:core.xml"})
public class DailyRateGetterApp {
    @Autowired
    LosRateTranslator losRateTranslator;

    @Autowired
    DailyRateGetter dailyRateGetter;

    @Test
    public void testGetFromPlugin() throws Exception {

        SimpleRQ simpleRQ = new SimpleRQ();
        simpleRQ.setHotelCode("RNODH");
        simpleRQ.setChannelCode("AGODA");
        simpleRQ.setCrNumbers(Sets.newHashSet("47161","47162","102083"));
        simpleRQ.setDate("2020-12-27");
        SaveDailyRateRequest saveDailyRateRequest = dailyRateGetter.getFromPlugin(simpleRQ);

        System.out.println(XStreamUtils.toXML(saveDailyRateRequest));


    }

    @Test
    public void testGetFromDstorage() throws Exception {

        SimpleRQ simpleRQ = new SimpleRQ();
        simpleRQ.setHotelCode("RNODH");
        simpleRQ.setChannelCode("AGODA");
        simpleRQ.setCrNumbers(Sets.newHashSet("47161","47162","102083"));
        simpleRQ.setDate("2020-11-04");
        SaveDailyRateRequest saveDailyRateRequest = dailyRateGetter.getFromDstorage(simpleRQ);

        System.out.println(XStreamUtils.toXML(saveDailyRateRequest));


    }
}
