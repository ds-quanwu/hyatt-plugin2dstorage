package com.derbysoft.hyatt.plugin2dstorage.exclude;

import com.derbysoft.common.util.xml.XStreamUtils;
import com.derbysoft.dswitch.core.util.TaskID;
import com.derbysoft.dswitch.dto.common.KeyValue;
import com.derbysoft.dswitch.dto.hotel.cds.DailyRateChangeRQ;
import com.derbysoft.dswitch.dto.hotel.cds.Type;
import com.derbysoft.dswitch.dto.hotel.common.DateRangeDTO;
import com.derbysoft.dswitch.remote.hotel.buyer.DefaultHotelBuyerRemoteService;
import com.derbysoft.dswitch.remote.hotel.dto.HotelDailyRateChangeRequest;
import com.derbysoft.dswitch.remote.hotel.dto.HotelDailyRateChangeResponse;
import com.derbysoft.dswitch.remote.hotel.dto.RequestHeader;
import com.derbysoft.dswitch.remote.hotel.dto.util.TpaExtensionsUtil;
import com.derbysoft.storage.dto.SaveDailyRateRQ;
import com.derbysoft.storage.dto.SaveRestrictionRQ;
import com.derbysoft.storage.dto.common.*;
import com.derbysoft.storage.dto.rate.daily.DailyRate;
import com.derbysoft.storage.dto.restriction.Inventory;
import com.derbysoft.storage.dto.restriction.Restrictions;
import com.derbysoft.storage.remote.buyer.StorageBuyerRemoteService;
import com.derbysoft.storage.remote.dto.SaveDailyRateRequest;
import com.derbysoft.storage.remote.dto.SaveDailyRateResponse;
import com.derbysoft.storage.remote.dto.SaveRestrictionRequest;
import com.derbysoft.storage.remote.dto.SaveRestrictionResponse;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class DstorageTest {
    @Test
    public void testSave() {
        StorageBuyerRemoteService storageBuyerRemoteService = new StorageBuyerRemoteService("10.110.207.236:7002");

        Restrictions restrictions = new Restrictions();
        restrictions.setLevel(Level.DateHotelRoomRateLevel);
        restrictions.setHotel("INDZC");
        restrictions.setDateRange(new DateRange("2020-11-01", "2020-11-02"));
        restrictions.setRatePlan("META_7FRE");
        restrictions.setRoomType("EXST");
        Inventory inventory = new Inventory();
        inventory.setFplos("8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8");
        restrictions.setInventory(inventory);

        List<Restrictions> restrictionList = new ArrayList<>();
        restrictionList.add(restrictions);

        SaveRestrictionRQ restrictionRQ = new SaveRestrictionRQ();
        restrictionRQ.setChannel("DPLATFORM");
        restrictionRQ.setProvider("HYATT");
        restrictionRQ.setRestrictionsList(restrictionList);

        SaveRestrictionRequest request = new SaveRestrictionRequest();
        request.setHeader(new RequestHeader("DPLATFORM", "HYATT", TaskID.generate()));
        request.setSaveRestrictionRQ(restrictionRQ);

        System.out.println(XStreamUtils.toXML(request));

        SaveRestrictionResponse response = storageBuyerRemoteService.save(request);
        System.out.println(XStreamUtils.toXML(response));


    }


    @Test
    public void testSaveDailyRate() {
        StorageBuyerRemoteService storageBuyerRemoteService = new StorageBuyerRemoteService("10.110.207.236:7002");
        List<DailyRate> rates = new ArrayList<>();

        DailyRate rate = new DailyRate();
        rate.setDateRange(new DateRange("2020-10-01", "2020-10-02"));
        rate.setCurrency("USD");
        rate.setRatePlan("META_15OG");
        rate.setRoomType("KING");
//        OccupancyRates occupancyRates = new OccupancyRates();
//        ArrayList<OccupancyRate> occupancyRate = new ArrayList<>();
//        OccupancyRate oe = new OccupancyRate();
//        oe.setAdult(1);
//        oe.setAmountAfterTax(333d);
//        occupancyRate.add(oe);
//        occupancyRates.setOccupancyRateList(occupancyRate);

        TpaExtensionsUtil.appendElements(rate, new KeyValue("RATE_CHANGE_INDICATOR", "true"));

        Rates r = new Rates();
//        r.setOccupancyRates(occupancyRates);

        rate.setRates(r);
        rates.add(rate);

        SaveDailyRateRQ dailyRQ = new SaveDailyRateRQ();
        dailyRQ.setChannel("DPLATFORM");
        dailyRQ.setProvider("HYATT");
        dailyRQ.setHotel("YYYYY");
        dailyRQ.setDailyRatesList(rates);

        SaveDailyRateRequest request = new SaveDailyRateRequest();
        request.setHeader(new RequestHeader("DPLATFORM", "HYATT", TaskID.generate()));
        request.setSaveDailyRateRQ(dailyRQ);

        System.out.println(XStreamUtils.toXML(request));

        SaveDailyRateResponse response = storageBuyerRemoteService.save(request);
        System.out.println(XStreamUtils.toXML(response));


    }


    @Test
    public void testGetDailyRate(){
        DefaultHotelBuyerRemoteService buyerRemoteService = new DefaultHotelBuyerRemoteService("10.110.207.236:9002");
        HotelDailyRateChangeRequest request = new HotelDailyRateChangeRequest();
        DailyRateChangeRQ dailyRateRQ = new DailyRateChangeRQ();
        dailyRateRQ.setHotelCode("RNODH");
        dailyRateRQ.setDateRange(new DateRangeDTO("2020-11-04", "2020-11-04"));
        dailyRateRQ.setType(Type.All);

        request.setHeader(new RequestHeader("AGODA", "HYATT", TaskID.generate()));
        request.setDailyRateChangeRQ(dailyRateRQ);
        HotelDailyRateChangeResponse response = buyerRemoteService.getDailyRateChange(request);
        System.out.println(XStreamUtils.toXML(response));

    }
}
