package com.derbysoft.hyatt.plugin2dstorage.exclude;

import com.derbysoft.schedulecenter.rpc.client.HttpScheduleCenterService;
import com.derbysoft.schedulecenter.rpc.protocol.*;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.SetMultimap;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;

import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.*;

public class ScheduleCenterTest {
    String target = "HYATT.PLUGIN_ARI_TO_DSTORAGE.MONTH";
//    HttpScheduleCenterService scheduleCenterService = new HttpScheduleCenterService("schedule-center-task-distributor.derbysoftapi.com/schedule-center-tasks");
    HttpScheduleCenterService scheduleCenterService = new HttpScheduleCenterService("schedule-center-distributor.derbysoft-test.com/schedule-center-tasks");

    @Test
    public void testGetTask() {


        List<Task> tasks = scheduleCenterService.exec(new GetTasks(target, 2));

        System.out.println(tasks.size());
        tasks.forEach(task -> {
            System.out.println(task.getId() + "=>" + task.getParametersMap());
            UpdateTaskStatus updateTaskStatus = new UpdateTaskStatus();
            updateTaskStatus.setTarget(target);
            updateTaskStatus.setFinishTaskId(Arrays.asList(task.getId()));
            scheduleCenterService.exec(updateTaskStatus);

        });
    }


    @Test
    public void testUpdate() {
//        1C244E855021DE96E07D8C9DF3F4D665

        UpdateTaskStatus updateTaskStatus = new UpdateTaskStatus();
        updateTaskStatus.setTarget(target);
        updateTaskStatus.setRunningTaskId(Arrays.asList());
        updateTaskStatus.setHoldTaskId(new ArrayList<>());
        updateTaskStatus.setFinishTaskId(Arrays.asList("B3725C6F502E2FEEC2C41CEC6110E66D"));
        scheduleCenterService.exec(updateTaskStatus);
    }

    @Test
    public void cosumerAllTask() {

        for (; ; ) {
            List<Task> tasks = scheduleCenterService.exec(new GetTasks(target, 100));

            tasks.forEach(task -> {
                UpdateTaskStatus updateTaskStatus = new UpdateTaskStatus();
                updateTaskStatus.setTarget(target);
                updateTaskStatus.setRunningTaskId(new ArrayList<>());
                updateTaskStatus.setHoldTaskId(new ArrayList<>());
                updateTaskStatus.setFinishTaskId(Arrays.asList(task.getId()));
                System.out.println(task.getId());
                scheduleCenterService.exec(updateTaskStatus);
            });
//            if (tasks.isEmpty()) {
//                break;
//            }
        }
    }

    @Test
    public void testCreate() {
        ArrayList<String> hotelCodes = Lists.newArrayList("BNAZB");
        ArrayList<String> list = Lists.newArrayList(
                "2020-06",
                "2020-07",
                "2020-08",
                "2020-09",
                "2020-10"
//                "2020-11",
//                "2020-12",
//                "2021-01",
//                "2021-02",
//                "2021-03",
//                "2021-04",
//                "2021-05",
//                "2021-06"
        );

        HashMultimap<String, String> updateLists = HashMultimap.create();

        for (String hotelCode : hotelCodes) {
            for (String s : list) {
                updateLists.put(hotelCode, s);
            }
        }

        createTask(updateLists);
    }


    @Test
    public void testCreateByIssuList() throws Exception {
        InputStream resourceAsStream = new ClassPathResource("issue.list").getInputStream();
        List<String> lines = IOUtils.readLines(resourceAsStream, Charset.defaultCharset());

        HashMultimap<String, String> hotelMonths = HashMultimap.create();
        for (String line : lines) {
            if (StringUtils.isBlank(line)) {
                continue;
            }
            String[] split = line.split("\\s");
            if (split.length != 2) {
                throw new IllegalStateException("Invalid line");
            }
            String hotelCode = split[0];
            String m = split[1].substring(0, 7);
            hotelMonths.put(hotelCode, m);
        }

        System.out.println(hotelMonths.size());

        hotelMonths.forEach((k, v) -> {
            System.out.println(k + "=" + v);
        });
        createTask(hotelMonths);

    }


    private void createTask(SetMultimap<String, String> hotelMonths) {
        for (String hotelCode : hotelMonths.keySet()) {
            Set<String> monTh = hotelMonths.get(hotelCode);
            for (String m : monTh) {
                List<Task> tasks = new ArrayList<>();
                Task t = new Task();
                t.setCreateTime(new Date().getTime());
                t.setId("Manual" + UUID.randomUUID().toString().replace("-", "").substring(6));
                Map<String, String> params = new HashMap<>();
                params.put("channelCode", "DPLATFORM");
                params.put("providerCode", "HYATT");
                params.put("hotelCode", hotelCode);
                params.put("maxLos", "30");
                params.put("crNumbers", "META");
                params.put("yearMonth", m);
                params.put("category", "buildRawRestriction");
                params.put("rateplans", "META_MYHI,META_RACK");
                t.setParametersMap(params);
                t.setPriority(5);
                tasks.add(t);

                CreateTasks createTasks = new CreateTasks();
                createTasks.setTarget(target);
                createTasks.setTasks(tasks);
                CreateTasksResult result = scheduleCenterService.exec(createTasks);
                System.out.println(result.isSuccess());
            }
        }
    }


}
