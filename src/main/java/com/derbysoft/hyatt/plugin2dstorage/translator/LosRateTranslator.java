package com.derbysoft.hyatt.plugin2dstorage.translator;

import com.derby.dswitch.ari.model.*;
import com.derby.dswitch.ari.wrap.GetAvailabilityChangeResponse;
import com.derbysoft.dswitch.core.util.TaskID;
import com.derbysoft.dswitch.remote.hotel.dto.RequestHeader;
import com.derbysoft.hyatt.plugin2dstorage.domain.DateRoomCategory;
import com.derbysoft.hyatt.plugin2dstorage.domain.LosAriData;
import com.derbysoft.hyatt.plugin2dstorage.domain.Promotion;
import com.derbysoft.hyatt.plugin2dstorage.domain.RoomCategory;
import com.derbysoft.hyatt.plugin2dstorage.repository.PromotionRepository;
import com.derbysoft.storage.dto.SaveLosRateRQ;
import com.derbysoft.storage.dto.SaveRestrictionRQ;
import com.derbysoft.storage.dto.common.DateRange;
import com.derbysoft.storage.dto.common.Level;
import com.derbysoft.storage.dto.common.OccupancyRates;
import com.derbysoft.storage.dto.common.Rates;
import com.derbysoft.storage.dto.rate.los.LosRate;
import com.derbysoft.storage.dto.rate.los.LosRates;
import com.derbysoft.storage.dto.restriction.Inventory;
import com.derbysoft.storage.dto.restriction.Restriction;
import com.derbysoft.storage.dto.restriction.Restrictions;
import com.derbysoft.storage.remote.dto.SaveLosRateRequest;
import com.derbysoft.storage.remote.dto.SaveRestrictionRequest;
import org.apache.commons.collections.CollectionUtils;
import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;
import java.util.function.Supplier;

@Service
public class LosRateTranslator {
    private static final Logger LOGGER = LoggerFactory.getLogger(LosRateTranslator.class);

    @Autowired
    private PromotionRepository promotionRepository;

    public SaveRestrictionRequest toRestrictionStatusRequest(LosAriData losAriData) {
        Map<DateRoomCategory, Map<Integer, Integer>> losInventories = losAriData.getLosInventories();
        if (losInventories.isEmpty()) {
            return null;
        }
        List<Restrictions> restrictionList = new ArrayList<>();

        for (Map.Entry<DateRoomCategory, Map<Integer, Integer>> entry : losInventories.entrySet()) {
            DateRoomCategory dateRoomCategory = entry.getKey();
            Map<Integer, Integer> losRoomcounts = entry.getValue();

            if (losRoomcounts.isEmpty()) {
                continue;
            }

            Integer curDayInv = losRoomcounts.getOrDefault(1, 0);
            List<String> roomCounts = new ArrayList<>();
            for (int i = 1; i <= losAriData.getMaxLos(); i++) {
                Integer inv = losRoomcounts.getOrDefault(i, 0);
                if (inv > 0) {
                    curDayInv = inv;
                }
                roomCounts.add(String.valueOf(inv > 0 ? 1 : 0));
            }
            Restriction restriction = new Restriction();
            restriction.setMaster(Restriction.Status.Open);
            if (curDayInv > 0 && "DPLATFORM".equals(losAriData.getChannelCode())) {
                restriction.setArrival(Restriction.Status.Open);
            }
            restriction.setFullPatternLos(String.join(",", roomCounts));

            Restrictions restrictions = new Restrictions();
            restrictions.setLevel(Level.DateHotelRoomRateLevel);
            restrictions.setHotel(losAriData.getHotelCode());
            restrictions.setDateRange(new DateRange(dateRoomCategory.getCheckIn().toString(), dateRoomCategory.getCheckIn().toString()));
            restrictions.setRatePlan(dateRoomCategory.getRatePlan());
            restrictions.setRoomType(dateRoomCategory.getRoomType());

            if (curDayInv > 0) {
                Inventory inventory = new Inventory();
                inventory.setInventory(curDayInv);
                restrictions.setInventory(inventory);
            }

            restrictions.setRestriction(restriction);

            restrictionList.add(restrictions);
        }

        if (restrictionList.isEmpty()) {
            return null;
        }

        SaveRestrictionRQ restrictionRQ = new SaveRestrictionRQ();
        restrictionRQ.setChannel(losAriData.getChannelCode());
        restrictionRQ.setProvider("HYATT");
        restrictionRQ.setRestrictionsList(restrictionList);

        SaveRestrictionRequest request = new SaveRestrictionRequest();
        request.setHeader(new RequestHeader(losAriData.getChannelCode(), "HYATT", TaskID.generate()));
        request.setSaveRestrictionRQ(restrictionRQ);

        return request;
    }


    public SaveRestrictionRequest toRestrictionRequest(LosAriData losAriData) {
        Map<DateRoomCategory, Map<Integer, Integer>> losInventories = losAriData.getLosInventories();
        if (losInventories.isEmpty()) {
            return null;
        }
        List<Restrictions> restrictionList = new ArrayList<>();

        for (Map.Entry<DateRoomCategory, Map<Integer, Integer>> entry : losInventories.entrySet()) {
            DateRoomCategory dateRoomCategory = entry.getKey();
            Map<Integer, Integer> losRoomcounts = entry.getValue();

            if (losRoomcounts.isEmpty()) {
                continue;
            }

            List<String> roomCounts = new ArrayList<>();
            for (int i = 1; i <= losAriData.getMaxLos(); i++) {
                roomCounts.add(String.valueOf(losRoomcounts.getOrDefault(i, 0)));
            }
            Restrictions restrictions = new Restrictions();
            restrictions.setLevel(Level.DateHotelRoomRateLevel);
            restrictions.setHotel(losAriData.getHotelCode());
            restrictions.setDateRange(new DateRange(dateRoomCategory.getCheckIn().toString(), dateRoomCategory.getCheckIn().toString()));
            restrictions.setRatePlan(dateRoomCategory.getRatePlan());
            restrictions.setRoomType(dateRoomCategory.getRoomType());
            Inventory inventory = new Inventory();
            inventory.setFplos(String.join(",", roomCounts));
            restrictions.setInventory(inventory);

            restrictionList.add(restrictions);
        }

        if (restrictionList.isEmpty()) {
            return null;
        }

        SaveRestrictionRQ restrictionRQ = new SaveRestrictionRQ();
        restrictionRQ.setChannel(losAriData.getChannelCode());
        restrictionRQ.setProvider("HYATT");
        restrictionRQ.setRestrictionsList(restrictionList);

        SaveRestrictionRequest request = new SaveRestrictionRequest();
        request.setHeader(new RequestHeader(losAriData.getChannelCode(), "HYATT", TaskID.generate()));
        request.setSaveRestrictionRQ(restrictionRQ);

        return request;
    }

    public SaveLosRateRequest toLosRateRequest(LosAriData losAriData) {
        Map<DateRoomCategory, Map<Integer, List<OccupancyRate>>> categoryLosRates = losAriData.getLosRates();
        if (categoryLosRates.isEmpty()) {
            return null;
        }

        List<LosRates> losRatesList = new ArrayList<>();
        for (Map.Entry<DateRoomCategory, Map<Integer, List<OccupancyRate>>> entry : categoryLosRates.entrySet()) {
            DateRoomCategory roomCategory = entry.getKey();
            Map<Integer, List<OccupancyRate>> losOccRates = entry.getValue();

            String currencyCode = null;
            List<LosRate> rates = new ArrayList<>();
            for (Map.Entry<Integer, List<OccupancyRate>> e : losOccRates.entrySet()) {
                Integer los = e.getKey();
                List<OccupancyRate> occRates = e.getValue();

                if (occRates.isEmpty()) {
                    continue;
                }

                if (los != occRates.size()) {
                    throw new IllegalStateException("missing occRates");
                }

                if (currencyCode == null) {
                    currencyCode = occRates.get(0).getCurrencyCode();
                }

                LosRate losRate = new LosRate();
                losRate.setLos(los);
                losRate.setRatesList(createRatesList(losAriData.getChannelCode(), losAriData.getHotelCode(), roomCategory, occRates));

                rates.add(losRate);
            }

            if (rates.isEmpty()) {
                continue;
            }

            LosRates losRatesItem = new LosRates();
            losRatesItem.setCheckin(roomCategory.getCheckIn().toString());
            losRatesItem.setRoomType(roomCategory.getRoomType());
            losRatesItem.setRatePlan(roomCategory.getRatePlan());
            losRatesItem.setCurrency(currencyCode);
            losRatesItem.setLosRateList(rates);

            losRatesList.add(losRatesItem);
        }

        SaveLosRateRQ saveLosRateRQ = new SaveLosRateRQ();
        saveLosRateRQ.setChannel(losAriData.getChannelCode());
        saveLosRateRQ.setProvider("HYATT");
        saveLosRateRQ.setHotel(losAriData.getHotelCode());
        saveLosRateRQ.setLosRatesList(losRatesList);
        saveLosRateRQ.setIsFullLos(true);

        SaveLosRateRequest saveLosRateRequest = new SaveLosRateRequest();
        saveLosRateRequest.setHeader(new RequestHeader(losAriData.getChannelCode(), "HYATT", TaskID.generate()));
        saveLosRateRequest.setSaveLosRateRQ(saveLosRateRQ);
        return saveLosRateRequest;
    }

    private List<Rates> createRatesList(String channelCode, String hotelCode, DateRoomCategory roomCategory, List<OccupancyRate> occRates) {
        List<Rates> rates = new ArrayList<>();

        for (OccupancyRate occRate : occRates) {
            String currencyCode = occRate.getCurrencyCode();
            List<Rate> nukeRate = occRate.getRates();

            if (isDiscounted(channelCode, hotelCode, roomCategory) || has0Rate(nukeRate)) {
                LOGGER.info("Discount True:{}-{}", hotelCode, roomCategory);
                nukeRate = getAvailRates(occRates);
            }

            List<com.derbysoft.storage.dto.common.OccupancyRate> guestRates = new ArrayList<>();

            for (Rate rate : nukeRate) {
                Integer guestCount = rate.getGuestCount();
                BigDecimal abt = rate.getAmountBeforeTax();
                BigDecimal aat = rate.getAmountAfterTax();

                com.derbysoft.storage.dto.common.OccupancyRate or = new com.derbysoft.storage.dto.common.OccupancyRate();
                or.setAdult(guestCount);
                or.setAmountBeforeTax(abt.doubleValue());
                if (aat != null) {
                    or.setAmountAfterTax(aat.doubleValue());
                }

                guestRates.add(or);
            }

            OccupancyRates dswitchOccRates = new OccupancyRates();
            dswitchOccRates.setOccupancyRateList(guestRates);

            Rates dswitchRate = new Rates();
            dswitchRate.setOccupancyRates(dswitchOccRates);
            rates.add(dswitchRate);
        }
        return rates;
    }

    private boolean has0Rate(List<Rate> nukeRate) {
        if (nukeRate == null || nukeRate.isEmpty()) {
            return true;
        }
        for (Rate rate : nukeRate) {
            BigDecimal abt = rate.getAmountBeforeTax();
            if (abt == null || BigDecimal.ZERO.compareTo(abt) == 0) {
                return true;
            }
        }
        return false;
    }

    private boolean isDiscounted(String channelCode, String hotelCode, DateRoomCategory roomCategory) {
        List<Promotion> hotelPromotions = promotionRepository.getHotelPromotions(channelCode, hotelCode);
        if (CollectionUtils.isEmpty(hotelPromotions)) {
            return false;
        }
        for (Promotion promotion : hotelPromotions) {
            if (promotion.getRatePlanCode().equals(roomCategory.getRatePlan()) &&
                    !roomCategory.getCheckIn().isBefore(promotion.getStart()) &&
                    !roomCategory.getCheckIn().isAfter(promotion.getEnd())) {
                return true;
            }
        }
        return false;
    }

    private List<Rate> getAvailRates(List<OccupancyRate> occRates) {
        for (OccupancyRate occRate : occRates) {
            List<Rate> rates = occRate.getRates();
            if (CollectionUtils.isNotEmpty(rates)) {
                BigDecimal amountBeforeTax = rates.get(0).getAmountBeforeTax();
                if (amountBeforeTax != null && BigDecimal.ZERO.compareTo(amountBeforeTax) != 0) {
                    return occRate.getRates();
                }
            }
        }
        throw new IllegalArgumentException("No rate");
    }

    public LosAriData toAriData(String channelCode, String hotelCode, int maxLos, Set<String> crNumbers,
                                GetAvailabilityChangeResponse response, Supplier<Set<RoomCategory>> productsSupplier) {
        AvailabilityUpdate availabilityUpdate = Optional.ofNullable(response)
                .map(GetAvailabilityChangeResponse::getAvailabilityUpdate)
                .orElse(null);


        if (availabilityUpdate == null || CollectionUtils.isEmpty(availabilityUpdate.getRoomStays())) {
            return null;
        }
        Set<RoomCategory> roomCategories = productsSupplier.get();

        List<RoomStay> roomStays = availabilityUpdate.getRoomStays();

        Map<DateRoomCategory, Map<Integer, Integer>> losInventoryMap = new HashMap<>();
        Map<DateRoomCategory, Map<Integer, List<OccupancyRate>>> losRateMap = new HashMap<>();

        for (RoomStay roomStay : roomStays) {
            String roomTypeCode = roomStay.getRoomTypeCode();
            String ratePlanCode = roomStay.getRatePlanCode();
            Integer lengthOfStay = roomStay.getLengthOfStay();
            Integer roomCount = roomStay.getRoomCount();
            DateSpan dateSpan = roomStay.getDateSpan();
            List<RoomRate> roomRates = roomStay.getRoomRates();

            boolean isValid = false;
            for (String crNumber : crNumbers) {
                if (ratePlanCode.contains(crNumber)) {
                    isValid = true;
                    break;
                }
            }
            if (!isValid) {
                continue;
            }

            if (!roomCategories.isEmpty() && !roomCategories.contains(new RoomCategory(roomTypeCode, ratePlanCode))) {
                LOGGER.info("Product size:[{}], not contain :[{}]", roomCategories.size(), new RoomCategory(roomTypeCode, ratePlanCode));
                continue;
            }

            for (LocalDate date = dateSpan.getStart(); !date.isAfter(dateSpan.getEnd()); date = date.plusDays(1)) {
                Map<Integer, Integer> losInventory = losInventoryMap.computeIfAbsent(new DateRoomCategory(roomTypeCode, ratePlanCode, date), k -> new HashMap<>());
                losInventory.put(lengthOfStay, roomCount);

                if (CollectionUtils.isNotEmpty(roomRates)) {
                    Map<Integer, OccupancyRate> dayRate = new TreeMap<>(Comparator.comparingInt(o -> o));
                    for (RoomRate roomRate : roomRates) {
                        List<Integer> days = roomRate.getDays();
                        OccupancyRate occupancyRate = roomRate.getOccupancyRate();
                        for (Integer day : days) {
                            dayRate.put(day, occupancyRate);
                        }
                    }
                    Map<Integer, List<OccupancyRate>> losRate = losRateMap.computeIfAbsent(new DateRoomCategory(roomTypeCode, ratePlanCode, date), k -> new HashMap<>());
                    List<OccupancyRate> rates = losRate.computeIfAbsent(lengthOfStay, k -> new ArrayList<>());
                    rates.addAll(dayRate.values());
                }
            }
        }

        LosAriData losAriData = new LosAriData(channelCode, hotelCode, maxLos);
        losAriData.setLosInventories(losInventoryMap);
        losAriData.setLosRates(losRateMap);
        return losAriData;
    }


}
