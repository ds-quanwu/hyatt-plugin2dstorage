package com.derbysoft.hyatt.plugin2dstorage.translator;

import com.derby.nuke.ari.model.DailyUpdate;
import com.derby.nuke.ari.model.DateSpan;
import com.derby.nuke.ari.model.SingleDailyUpdate;
import com.derby.nuke.ari.wrap.GetDailyChangeResponse;
import com.derbysoft.dswitch.core.util.TaskID;
import com.derbysoft.dswitch.remote.hotel.dto.RequestHeader;
import com.derbysoft.hyatt.plugin2dstorage.domain.RoomCategory;
import com.derbysoft.storage.dto.SaveRestrictionRQ;
import com.derbysoft.storage.dto.common.DateRange;
import com.derbysoft.storage.dto.common.Level;
import com.derbysoft.storage.dto.restriction.Inventory;
import com.derbysoft.storage.dto.restriction.Restrictions;
import com.derbysoft.storage.remote.dto.SaveRestrictionRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Supplier;


@Service
public class DailyRateTranslator {
    private static final Logger LOGGER = LoggerFactory.getLogger(DailyRateTranslator.class);


    public SaveRestrictionRequest toDailyInventory(
            String channel,
            String hotelCode,
            Set<String> crNumbers,
            GetDailyChangeResponse response,
            Supplier<Set<RoomCategory>> productsSupplier) {

        List<SingleDailyUpdate> singleDailyUpdates = Optional.ofNullable(response)
                .map(GetDailyChangeResponse::getDailyUpdate)
                .map(DailyUpdate::getSingleDailyUpdates)
                .orElse(null);

        if (singleDailyUpdates == null) {
            return null;
        }

        Set<RoomCategory> roomCategories = productsSupplier.get();

        List<Restrictions> restrictionList = new ArrayList<>();
        for (SingleDailyUpdate singleDailyUpdate : singleDailyUpdates) {
            String roomTypeCode = singleDailyUpdate.getRoomTypeCode();
            String ratePlanCode = singleDailyUpdate.getRatePlanCode();
            Integer roomCount = singleDailyUpdate.getRoomCount();
            DateSpan dateSpan = singleDailyUpdate.getDateSpan();
            if (roomCount == null || roomCount < 1) {
                continue;
            }

            boolean isValid = false;
            for (String crNumber : crNumbers) {
                if (ratePlanCode.contains(crNumber)) {
                    isValid = true;
                    break;
                }
            }
            if (!isValid) {
                continue;
            }

            if (!roomCategories.isEmpty() && !roomCategories.contains(new RoomCategory(roomTypeCode, ratePlanCode))) {
                LOGGER.info("Product size:[{}], not contain :[{}]", roomCategories.size(), new RoomCategory(roomTypeCode, ratePlanCode));
                continue;
            }

            Restrictions restrictions = new Restrictions();
            restrictions.setLevel(Level.DateHotelRoomRateLevel);
            restrictions.setHotel(hotelCode);
            restrictions.setDateRange(new DateRange(dateSpan.getStart().toString(), dateSpan.getEnd().toString()));
            restrictions.setRatePlan(ratePlanCode);
            restrictions.setRoomType(roomTypeCode);
            Inventory inventory = new Inventory();
            inventory.setInventory(roomCount);
            restrictions.setInventory(inventory);

            restrictionList.add(restrictions);
        }

        SaveRestrictionRQ restrictionRQ = new SaveRestrictionRQ();
        restrictionRQ.setChannel(channel);
        restrictionRQ.setProvider("HYATT");
        restrictionRQ.setRestrictionsList(restrictionList);

        SaveRestrictionRequest request = new SaveRestrictionRequest();
        request.setHeader(new RequestHeader(channel, "HYATT", TaskID.generate()));
        request.setSaveRestrictionRQ(restrictionRQ);

        return request;

    }

}
