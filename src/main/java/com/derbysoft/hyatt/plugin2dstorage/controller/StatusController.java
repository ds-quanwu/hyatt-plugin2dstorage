package com.derbysoft.hyatt.plugin2dstorage.controller;

import com.derby.dswitch.ari.model.Contract;
import com.derby.dswitch.ari.model.DateSpan;
import com.derby.dswitch.ari.model.RetrieveChangeCriteria;
import com.derby.dswitch.ari.wrap.GetAvailabilityChangeRequest;
import com.derby.nuke.ari.wrap.GetDailyChangeRequest;
import com.derbysoft.hyatt.plugin2dstorage.dto.SimpleRQ;
import com.derbysoft.hyatt.plugin2dstorage.service.DstorageProductService;
import com.derbysoft.hyatt.plugin2dstorage.task.BatchTaskFetcher;
import com.derbysoft.hyatt.plugin2dstorage.task.HyattPluginData2DstorageTask;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.CompletableFuture;

@RestController
public class StatusController {
    private static Logger LOGGER = LoggerFactory.getLogger(StatusController.class);

    @Autowired
    private BatchTaskFetcher batchTaskFetcher;

    @Autowired
    private HyattPluginData2DstorageTask hyattPluginData2DstorageTask;

    @Autowired
    private DstorageProductService dstorageProductService;

    @GetMapping(value = "/fetch/{status}")
    public Object openOrCloseFetch(@PathVariable("status") String status) {
        batchTaskFetcher.setClose(Boolean.parseBoolean(status));
        return true;
    }

    @GetMapping(value = "/filter/{status}")
    public Object filter(@PathVariable("status") String status) {
        batchTaskFetcher.setFilter(Boolean.parseBoolean(status));
        return true;
    }

    @GetMapping(value = "/cache/clean")
    public Object cacheClean() {
        dstorageProductService.cleanCache();
        return true;
    }

    @GetMapping(value = "/fetch/step/{step}")
    public Object step(@PathVariable("step") int step) {
        if (step >= 1) {
            hyattPluginData2DstorageTask.setStep(step);
        }
        return true;
    }

    @GetMapping(value = "/thread/{size}")
    public Object changeThreadSize(@PathVariable("size") int size) {
        batchTaskFetcher.setPoolSize(size);
        return true;
    }

    @PostMapping(value = "/sync")
    public Object sync(@RequestBody SimpleRQ simpleRQ) {
        LOGGER.info("submit by manual...");
        Set<Integer> losList = new HashSet<>();
        for (int i = 1; i <= simpleRQ.getMaxLos(); i++) {
            losList.add(i);
        }

        GetAvailabilityChangeRequest request = new GetAvailabilityChangeRequest();
        request.setContract(new Contract(simpleRQ.getChannelCode(), "HYATT"));
        request.setToken("Hyatt-plugin2dstorage");
        RetrieveChangeCriteria criteria = new RetrieveChangeCriteria();
        criteria.setHotelCode(simpleRQ.getHotelCode());
        criteria.setDateSpan(new DateSpan(new LocalDate(simpleRQ.getDate()), new LocalDate(simpleRQ.getDate())));
        criteria.setLosCandidates(losList);
        criteria.setType("All");
        criteria.setProperty("availability.all", "true");
        criteria.setProperty("los.all", "true");
        criteria.setProperty("los.roomcount", "true");
        criteria.setProperty("categories.all", "false");
        criteria.setProperty("cancel.policy.required", "false");
        request.setRetrieveChangeCriteria(criteria);

        GetDailyChangeRequest dailyChangeRequest = new GetDailyChangeRequest();
        dailyChangeRequest.setContract(new com.derby.nuke.ari.model.Contract(simpleRQ.getChannelCode(), "HYATT"));
        com.derby.nuke.ari.model.RetrieveChangeCriteria daiyCriteria = new com.derby.nuke.ari.model.RetrieveChangeCriteria();
        daiyCriteria.setHotelCode(simpleRQ.getHotelCode());
        daiyCriteria.setType("All");
        daiyCriteria.setDateSpan(new com.derby.nuke.ari.model.DateSpan(new LocalDate(simpleRQ.getDate()), new LocalDate(simpleRQ.getDate())));
        daiyCriteria.setRequestCategories(Sets.newHashSet(
                "LosInventory",
                "Inventory",
                "Restriction",
                "Rate"
        ));
        dailyChangeRequest.setRetrieveChangeCriteria(daiyCriteria);

        CompletableFuture.runAsync(() -> batchTaskFetcher.fetch(simpleRQ.getChannelCode(), simpleRQ.getHotelCode(),
                simpleRQ.getMaxLos(), simpleRQ.getCrNumbers(),
                Lists.newArrayList(request),
                Lists.newArrayList(dailyChangeRequest),
                simpleRQ.getCategory()));

        return true;
    }

}
