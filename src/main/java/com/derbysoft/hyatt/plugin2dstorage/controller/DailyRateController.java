package com.derbysoft.hyatt.plugin2dstorage.controller;

import com.derbysoft.common.util.xml.XStreamUtils;
import com.derbysoft.hyatt.plugin2dstorage.dto.SimpleRQ;
import com.derbysoft.hyatt.plugin2dstorage.executor.DailyRateGetter;
import com.derbysoft.hyatt.plugin2dstorage.service.StorageService;
import com.derbysoft.storage.remote.dto.SaveDailyRateRequest;
import com.derbysoft.storage.remote.dto.SaveDailyRateResponse;
import com.google.common.collect.ImmutableMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DailyRateController {

    @Autowired
    private DailyRateGetter dailyRateGetter;

    @Autowired
    private StorageService storageService;

    @PostMapping(value = "/dailyRate/set/indicator")
    public Object setIndicator(@RequestBody SimpleRQ simpleRQ) {
        SaveDailyRateRequest rates = dailyRateGetter.getFromDstorage(simpleRQ);
        if (rates == null) {
            return ImmutableMap.of("result", "ERROR", "message", "No dailyRate");
        }
        SaveDailyRateResponse saveDailyRateResponse = storageService.saveRawDailyRates(rates);
        if (saveDailyRateResponse.hasError()) {
            return ImmutableMap.of("result", "ERROR", "message", XStreamUtils.toXML(saveDailyRateResponse));
        }
        return ImmutableMap.of("result", "SUCCESS", "message", XStreamUtils.toXML(saveDailyRateResponse));
    }

    @PostMapping(value = "/dailyRate/sync/fromPlugin")
    public Object syncFromPlugin(@RequestBody SimpleRQ simpleRQ) {
        SaveDailyRateRequest rates = dailyRateGetter.getFromPlugin(simpleRQ);
        if (rates == null) {
            return ImmutableMap.of("result", "ERROR", "message", "No dailyRate");
        }
        SaveDailyRateResponse saveDailyRateResponse = storageService.saveRawDailyRates(rates);
        if (saveDailyRateResponse.hasError()) {
            return ImmutableMap.of("result", "ERROR", "message", XStreamUtils.toXML(saveDailyRateResponse));
        }
        return ImmutableMap.of("result", "SUCCESS", "message", XStreamUtils.toXML(saveDailyRateResponse));
    }


}
