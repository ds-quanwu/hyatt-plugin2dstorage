package com.derbysoft.hyatt.plugin2dstorage.controller;

import com.derbysoft.common.util.xml.XStreamUtils;
import com.derbysoft.dswitch.core.util.TaskID;
import com.derbysoft.dswitch.remote.hotel.dto.RequestHeader;
import com.derbysoft.hyatt.plugin2dstorage.dto.RestrictionRQ;
import com.derbysoft.hyatt.plugin2dstorage.service.StorageService;
import com.derbysoft.storage.dto.SaveRestrictionRQ;
import com.derbysoft.storage.dto.common.DateRange;
import com.derbysoft.storage.dto.common.Level;
import com.derbysoft.storage.dto.restriction.Inventory;
import com.derbysoft.storage.dto.restriction.Restriction;
import com.derbysoft.storage.dto.restriction.Restrictions;
import com.derbysoft.storage.remote.dto.SaveRestrictionRequest;
import com.derbysoft.storage.remote.dto.SaveRestrictionResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.Optional;

@RestController
public class RestrictionController {

    @Autowired
    private StorageService storageService;

    @PostMapping(value = "/restriction/set")
    public Object setRestriction(@RequestBody RestrictionRQ restrictionRQ) {
        if (restrictionRQ.getInventory() == null && !restrictionRQ.hasRestriction()) {
            return false;
        }

        Restrictions restrictions = new Restrictions();
        restrictions.setLevel(Level.DateHotelRoomRateLevel);
        restrictions.setHotel(restrictionRQ.getHotelCode());
        restrictions.setDateRange(new DateRange(restrictionRQ.getStart(), restrictionRQ.getEnd()));
        restrictions.setRatePlan(restrictionRQ.getRatePlan());
        restrictions.setRoomType(restrictionRQ.getRoomType());

        if (restrictionRQ.getInventory() != null) {
            Inventory inventory = new Inventory();
            inventory.setInventory(restrictionRQ.getInventory());
            restrictions.setInventory(inventory);
        }

        if (restrictionRQ.hasRestriction()) {
            Restriction restriction = new Restriction();
            Optional.ofNullable(restrictionRQ.getMaster()).ifPresent(master -> restriction.setMaster(master ? Restriction.Status.Open : Restriction.Status.Close));
            Optional.ofNullable(restrictionRQ.getArrival()).ifPresent(arrival -> restriction.setArrival(arrival ? Restriction.Status.Open : Restriction.Status.Close));
            Optional.ofNullable(restrictionRQ.getFplos()).ifPresent(restriction::setFullPatternLos);
            restrictions.setRestriction(restriction);
        }

        SaveRestrictionRQ restrictionRequest = new SaveRestrictionRQ();
        restrictionRequest.setChannel(restrictionRQ.getChannelCode());
        restrictionRequest.setProvider("HYATT");
        restrictionRequest.setRestrictionsList(Arrays.asList(restrictions));

        SaveRestrictionRequest request = new SaveRestrictionRequest();
        request.setHeader(new RequestHeader(restrictionRQ.getChannelCode(), "HYATT", TaskID.generate()));
        request.setSaveRestrictionRQ(restrictionRequest);
        SaveRestrictionResponse saveRestrictionResponse = storageService.saveRawRestriction(request);

        return XStreamUtils.toXML(saveRestrictionResponse);
    }

}
