package com.derbysoft.hyatt.plugin2dstorage.domain;


import com.derbysoft.ccs.core.MappingEntityDisabledField;
import org.joda.time.LocalDate;

import java.math.BigDecimal;
import java.text.MessageFormat;

public class Promotion implements MappingEntityDisabledField {
    public static final String RULE_TYPE_LAST = "Last";
    public static final String RULE_TYPE_LOWEST = "Lowest";

    private String hotelCode;
    private String channelCode;
    private String ratePlanCode;
    private LocalDate start;
    private LocalDate end;
    private Integer minStay;
    private Boolean superposition;
    private String rule;
    private BigDecimal discount;
    private boolean disabled;

    public String getHotelCode() {
        return hotelCode;
    }

    public void setHotelCode(String hotelCode) {
        this.hotelCode = hotelCode;
    }

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public String getRatePlanCode() {
        return ratePlanCode;
    }

    public void setRatePlanCode(String ratePlanCode) {
        this.ratePlanCode = ratePlanCode;
    }

    public LocalDate getStart() {
        return start;
    }

    public void setStart(LocalDate start) {
        this.start = start;
    }

    public LocalDate getEnd() {
        return end;
    }

    public void setEnd(LocalDate end) {
        this.end = end;
    }

    public Integer getMinStay() {
        return minStay;
    }

    public void setMinStay(Integer minStay) {
        this.minStay = minStay;
    }

    public Boolean getSuperposition() {
        return superposition;
    }

    public void setSuperposition(Boolean superposition) {
        this.superposition = superposition;
    }

    public void setRule(String rule) {
        this.rule = rule;
    }

    public String getRule() {
        return rule;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    @Override
    public String toString() {
        return MessageFormat.format("{0}|{1}|{2}|MinStay[{3}]|Rule[{4}]|Discount[{5}]|Superposition[{6}]", ratePlanCode, start, end, minStay, rule, discount, superposition);
    }

    @Override
    public void setDisabled(boolean b) {
        this.disabled = b;
    }

    @Override
    public boolean isDisabled() {
        return this.disabled;
    }
}
