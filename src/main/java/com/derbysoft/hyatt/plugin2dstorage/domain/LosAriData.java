package com.derbysoft.hyatt.plugin2dstorage.domain;

import com.derby.dswitch.ari.model.OccupancyRate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LosAriData {
    private String channelCode;
    private String hotelCode;
    private int maxLos;

    private Map<DateRoomCategory, Map<Integer, Integer>> losInventories = new HashMap<>();
    private Map<DateRoomCategory, Map<Integer, List<OccupancyRate>>> losRates = new HashMap<>();

    public LosAriData(String channelCode, String hotelCode, int maxLos) {
        this.channelCode = channelCode;
        this.hotelCode = hotelCode;
        this.maxLos = maxLos;
    }

    //    private Map<com.derbysoft.hyatt.plugin2dstorage.domain.LosRoomCategory, Integer> losInventories = new HashMap<>();
//    private Map<LosRoomCategory, List<OccupancyRate>> losRates = new HashMap<>();

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public String getHotelCode() {
        return hotelCode;
    }

    public void setHotelCode(String hotelCode) {
        this.hotelCode = hotelCode;
    }

    public Map<DateRoomCategory, Map<Integer, Integer>> getLosInventories() {
        return losInventories;
    }

    public void setLosInventories(Map<DateRoomCategory, Map<Integer, Integer>> losInventories) {
        this.losInventories = losInventories;
    }

    public Map<DateRoomCategory, Map<Integer, List<OccupancyRate>>> getLosRates() {
        return losRates;
    }

    public void setLosRates(Map<DateRoomCategory, Map<Integer, List<OccupancyRate>>> losRates) {
        this.losRates = losRates;
    }

    public int getMaxLos() {
        return maxLos;
    }

    public void setMaxLos(int maxLos) {
        this.maxLos = maxLos;
    }
}
