package com.derbysoft.hyatt.plugin2dstorage.domain;

import com.google.common.base.MoreObjects;

import java.util.Objects;

public class RoomCategory {
    private String roomType;
    private String ratePlan;

    public RoomCategory(String roomType, String ratePlan) {
        this.roomType = roomType;
        this.ratePlan = ratePlan;
    }

    public String getRoomType() {
        return roomType;
    }

    public String getRatePlan() {
        return ratePlan;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RoomCategory that = (RoomCategory) o;
        return Objects.equals(roomType, that.roomType) &&
                Objects.equals(ratePlan, that.ratePlan);
    }

    @Override
    public int hashCode() {
        return Objects.hash(roomType, ratePlan);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("roomType", roomType)
                .add("ratePlan", ratePlan)
                .toString();
    }
}
