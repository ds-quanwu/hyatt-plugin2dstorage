package com.derbysoft.hyatt.plugin2dstorage.domain;

import com.google.common.base.MoreObjects;
import org.joda.time.LocalDate;

import java.util.Objects;

public class DateRoomCategory {

    private String roomType;
    private String ratePlan;
    private LocalDate checkIn;

    public DateRoomCategory(String roomType, String ratePlan, LocalDate checkIn) {
        this.roomType = roomType;
        this.ratePlan = ratePlan;
        this.checkIn = checkIn;
    }

    public String getRoomType() {
        return roomType;
    }

    public String getRatePlan() {
        return ratePlan;
    }

    public LocalDate getCheckIn() {
        return checkIn;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("roomType", roomType)
                .add("ratePlan", ratePlan)
                .add("checkIn", checkIn)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DateRoomCategory that = (DateRoomCategory) o;
        return Objects.equals(roomType, that.roomType) &&
                Objects.equals(ratePlan, that.ratePlan) &&
                Objects.equals(checkIn, that.checkIn);
    }

    @Override
    public int hashCode() {
        return Objects.hash(roomType, ratePlan, checkIn);
    }
}
