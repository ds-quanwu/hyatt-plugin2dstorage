package com.derbysoft.hyatt.plugin2dstorage.dto;

import java.util.Set;

public class SimpleRQ {
    private String channelCode;
    private String hotelCode;
    private String date;
    private int maxLos;
    private Set<String> crNumbers;
    private String category;

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public String getHotelCode() {
        return hotelCode;
    }

    public void setHotelCode(String hotelCode) {
        this.hotelCode = hotelCode;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getMaxLos() {
        return maxLos;
    }

    public void setMaxLos(int maxLos) {
        this.maxLos = maxLos;
    }

    public Set<String> getCrNumbers() {
        return crNumbers;
    }

    public void setCrNumbers(Set<String> crNumbers) {
        this.crNumbers = crNumbers;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCategory() {
        return category;
    }
}
