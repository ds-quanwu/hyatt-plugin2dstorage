package com.derbysoft.hyatt.plugin2dstorage.service;

import com.derbysoft.storage.remote.buyer.StorageBuyerRemoteService;
import com.derbysoft.storage.remote.dto.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Optional;

@Service
public class StorageService {
    private static final Logger LOGGER = LoggerFactory.getLogger(StorageService.class);

    @Resource(name = "storageBuildSaveRepository")
    private StorageBuyerRemoteService storageBuildRepository;

    @Resource(name = "storageRawSaveRepository")
    private StorageBuyerRemoteService storageRawRepository;

    public SaveLosRateResponse saveLosRate(SaveLosRateRequest saveLosRateRequest) {
        SaveLosRateResponse response = storageBuildRepository.save(saveLosRateRequest);
        Optional.ofNullable(response)
                .map(SaveLosRateResponse::getSaveLosRateRS)
                .ifPresent(saveLosRateRS -> LOGGER.info("save los rate success, changed:{}", saveLosRateRS.getHasChanged()));
        return response;
    }

    public SaveRestrictionResponse saveRestriction(SaveRestrictionRequest saveRestrictionRequest) {
        SaveRestrictionResponse response = storageBuildRepository.save(saveRestrictionRequest);
        Optional.ofNullable(response)
                .map(SaveRestrictionResponse::getSaveRestrictionRS)
                .ifPresent(saveRestrictionRS -> LOGGER.info("save restriction success, changed:{}", saveRestrictionRS.getHasChanged()));
        return response;
    }

    public SaveRestrictionResponse saveRawRestriction(SaveRestrictionRequest saveRestrictionRequest) {
        SaveRestrictionResponse response = storageRawRepository.save(saveRestrictionRequest);
        Optional.ofNullable(response)
                .map(SaveRestrictionResponse::getSaveRestrictionRS)
                .ifPresent(saveRestrictionRS -> LOGGER.info("save raw restriction success, changed:{}", saveRestrictionRS.getHasChanged()));
        return response;
    }

    public SaveDailyRateResponse saveRawDailyRates(SaveDailyRateRequest saveDailyRateRequest) {
        SaveDailyRateResponse response = storageRawRepository.save(saveDailyRateRequest);
        Optional.ofNullable(response)
                .map(SaveDailyRateResponse::getSaveDailyRateRS)
                .ifPresent(saveRestrictionRS -> LOGGER.info("save raw daily rate success, changed:{}", saveRestrictionRS.getHasChanged()));
        return response;
    }

    public GetRoomRateLastDateResponse getDailyRoomRates(GetRoomRateLastDateRequest getRoomRateLastDateRequest) {
        return storageRawRepository.getRoomRates(getRoomRateLastDateRequest);
    }


}