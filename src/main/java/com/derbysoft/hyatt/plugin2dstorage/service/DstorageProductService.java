package com.derbysoft.hyatt.plugin2dstorage.service;

import com.derbysoft.dswitch.core.util.TaskID;
import com.derbysoft.dswitch.remote.hotel.dto.RequestHeader;
import com.derbysoft.dswitch.remote.hotel.exception.ExceptionFactory;
import com.derbysoft.hyatt.plugin2dstorage.domain.RoomCategory;
import com.derbysoft.storage.dto.GetRoomRateLastDateRQ;
import com.derbysoft.storage.dto.roomrate.RoomRateLastDate;
import com.derbysoft.storage.remote.dto.GetRoomRateLastDateRequest;
import com.derbysoft.storage.remote.dto.GetRoomRateLastDateResponse;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

@Service
public class DstorageProductService {
    private static final Logger LOGGER = LoggerFactory.getLogger(DstorageProductService.class);

    @Autowired
    private StorageService storageService;
    private Cache<String, Set<RoomCategory>> caches = CacheBuilder.newBuilder()
            .expireAfterWrite(5, TimeUnit.MINUTES)
            .initialCapacity(1200)
            .build();

    public Set<RoomCategory> getProducts(String channelCode, String hotelCode) {
        try {
            return caches.get(key(channelCode, hotelCode), () -> {
                try {
                    return getAllProduct(channelCode, hotelCode);
                } catch (Exception e) {
                    LOGGER.error("GetProduct fail:", e);
                    return new HashSet<>();
                }
            });
        } catch (Exception e) {
            LOGGER.error("GetProduct fail:", e);
            return new HashSet<>();
        }
    }

    private String key(String channelCode, String hotelCode) {
        return channelCode + hotelCode;
    }

    private Set<RoomCategory> getAllProduct(String channel, String hotelCode) {
        if (StringUtils.isBlank(channel) || StringUtils.isBlank(hotelCode))
            return new HashSet<>();

        GetRoomRateLastDateResponse roomRates = storageService.getDailyRoomRates(createRateLastDateRQ(channel, hotelCode));
        if (roomRates != null && roomRates.hasError())
            throw ExceptionFactory.create(roomRates.getError());

        if (isEmptyProduct(roomRates))
            return new HashSet<>();

        Set<RoomCategory> result = new HashSet<>();
        List<RoomRateLastDate> roomRateLastDatesList = roomRates.getGetRoomRateLastDateRS().getRoomRateLastDatesList();
        for (RoomRateLastDate product : roomRateLastDatesList) {
            if (!product.getRatePlan().contains("_")) {
                //Invalid rateplan
                continue;
            }
            result.add(new RoomCategory(product.getRoomType(), product.getRatePlan()));
        }

        return result;
    }

    private GetRoomRateLastDateRequest createRateLastDateRQ(String channel, String hotel) {
        GetRoomRateLastDateRequest rq = new GetRoomRateLastDateRequest();
        rq.setHeader(new RequestHeader(channel, "HYATT", TaskID.generate()));
        GetRoomRateLastDateRQ getRoomRateLastDateRQ = new GetRoomRateLastDateRQ();
        getRoomRateLastDateRQ.setChannel(channel);
        getRoomRateLastDateRQ.setHotel(hotel);
        getRoomRateLastDateRQ.setProvider("HYATT");
        rq.setGetRoomRateLastDateRQ(getRoomRateLastDateRQ);
        return rq;
    }

    private boolean isEmptyProduct(GetRoomRateLastDateResponse roomRates) {
        if (null == roomRates || null == roomRates.getGetRoomRateLastDateRS())
            return true;

        return CollectionUtils.isEmpty(roomRates.getGetRoomRateLastDateRS().getRoomRateLastDatesList());
    }

    public void cleanCache(){
        caches.invalidateAll();
        caches.cleanUp();
    }
}
