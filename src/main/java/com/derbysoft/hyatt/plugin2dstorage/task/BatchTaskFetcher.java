package com.derbysoft.hyatt.plugin2dstorage.task;

import com.derby.dswitch.ari.wrap.GetAvailabilityChangeRequest;
import com.derby.dswitch.ari.wrap.GetAvailabilityChangeResponse;
import com.derby.nuke.ari.wrap.GetDailyChangeRequest;
import com.derby.nuke.ari.wrap.GetDailyChangeResponse;
import com.derbysoft.hyatt.plugin2dstorage.domain.LosAriData;
import com.derbysoft.hyatt.plugin2dstorage.domain.RoomCategory;
import com.derbysoft.hyatt.plugin2dstorage.service.DstorageProductService;
import com.derbysoft.hyatt.plugin2dstorage.service.StorageService;
import com.derbysoft.hyatt.plugin2dstorage.translator.DailyRateTranslator;
import com.derbysoft.hyatt.plugin2dstorage.translator.LosRateTranslator;
import com.derbysoft.hyatt.plugin2dstorage.web.PluginWebService;
import com.derbysoft.storage.remote.dto.SaveLosRateRequest;
import com.derbysoft.storage.remote.dto.SaveRestrictionRequest;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PreDestroy;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.*;

@Service
public class BatchTaskFetcher {
    private static final Logger LOGGER = LoggerFactory.getLogger(BatchTaskFetcher.class);

    private volatile boolean close = false;

    @Autowired
    private PluginWebService pluginWebService;

    @Autowired
    private LosRateTranslator losRateTranslator;

    @Autowired
    private DailyRateTranslator dailyRateTranslator;

    @Autowired
    private StorageService storageService;

    @Autowired
    private DstorageProductService dstorageProductService;

    private ThreadPoolExecutor threadPool;

    private volatile boolean filter = false;

    public BatchTaskFetcher() {
        threadPool = new ThreadPoolExecutor(2, 3,
                0L, TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<>());
    }


    public void fetch(
            String channelCode,
            String hotelCode,
            int maxLos,
            Set<String> crNumbers,
            List<GetAvailabilityChangeRequest> requests,
            List<GetDailyChangeRequest> dailyChangeRequests,
            String category) {

        if (close) {
            return;
        }
        long start = System.currentTimeMillis();
        final Set<RoomCategory> products = new HashSet<>();
        if (filter) {
            products.addAll(dstorageProductService.getProducts(channelCode, hotelCode));
        }

        List<Callable<Void>> tasks = new ArrayList<>();
        for (GetAvailabilityChangeRequest request : requests) {
            Callable<Void> task = () -> {
                try {
                    if (StringUtils.isBlank(category) || "DailyInventory".equals(category)) {
                        return null;
                    }

                    GetAvailabilityChangeResponse availabilityChangeResponse = pluginWebService.getLosRate(request);
                    LosAriData losAriData = losRateTranslator.toAriData(channelCode, hotelCode, maxLos, crNumbers, availabilityChangeResponse, () -> products);

                    SaveLosRateRequest losRateRequest = losRateTranslator.toLosRateRequest(losAriData);
                    SaveRestrictionRequest restrictionRequest = losRateTranslator.toRestrictionRequest(losAriData);
                    SaveRestrictionRequest restrictionStatusRequest = losRateTranslator.toRestrictionStatusRequest(losAriData);

                    if (losRateRequest != null && category.contains("LosRate")) {
                        storageService.saveLosRate(losRateRequest);
                    }

                    if (restrictionRequest != null && category.contains("buildRestriction")) {
                        storageService.saveRestriction(restrictionRequest);
                    }

                    if (restrictionStatusRequest != null && category.contains("buildRawRestriction")) {
                        storageService.saveRawRestriction(restrictionStatusRequest);
                    }

                    LOGGER.info("Save los: {}|{}|{}|{}|{}, suc", hotelCode, maxLos, crNumbers, request.getRetrieveChangeCriteria().getDateSpan().getStart(), category);
                } catch (Exception e) {
                    LOGGER.error("Save los: {}|{}|{}|{}|{}, failed " + ExceptionUtils.getStackTrace(e), hotelCode, maxLos, crNumbers, request.getRetrieveChangeCriteria().getDateSpan().getStart(), category);
                }
                return null;
            };
            tasks.add(task);
        }

        for (GetDailyChangeRequest request : dailyChangeRequests) {
            tasks.add(() -> {
                try {
                    if (StringUtils.isBlank(category) || !category.contains(HyattPluginData2DstorageTask.category_type_dailyInv)) {
                        return null;
                    }

                    GetDailyChangeResponse response = pluginWebService.getDailyRate(request);
                    SaveRestrictionRequest saveRestrictionRequest = dailyRateTranslator.toDailyInventory(channelCode, hotelCode, crNumbers, response, () -> products);
                    if (saveRestrictionRequest != null) {
                        storageService.saveRawRestriction(saveRestrictionRequest);
                    } else {
                        LOGGER.warn("Save daily fail no avail: {}|{}|{}|{}, suc", hotelCode, crNumbers, request.getRetrieveChangeCriteria().getDateSpan().getStart(), category);
                    }
                    LOGGER.info("Save daily: {}|{}|{}|{}, suc", hotelCode, crNumbers, request.getRetrieveChangeCriteria().getDateSpan().getStart(), category);
                } catch (Exception e) {
                    LOGGER.error("Save daily: {}|{}|{}|{}, failed " + ExceptionUtils.getStackTrace(e), hotelCode, crNumbers, request.getRetrieveChangeCriteria().getDateSpan().getStart(), category);
                }

                return null;
            });
        }


        try {
            List<Future<Void>> futures = threadPool.invokeAll(tasks);
            for (Future<Void> future : futures) {
                future.get();
            }
        } catch (Exception e) {
            LOGGER.error(ExceptionUtils.getStackTrace(e));
        }

        LOGGER.info("task cost:{}", System.currentTimeMillis() - start);
    }

    public void setClose(boolean close) {
        this.close = close;
    }

    public void setPoolSize(int size) {
        threadPool.setCorePoolSize(size);
        threadPool.setMaximumPoolSize(size + 1);
        LOGGER.info("Change thread size:{}", size);
    }

    @PreDestroy
    public void destroy() {
        threadPool.shutdownNow();
    }

    public void setFilter(boolean filter) {
        this.filter = filter;
    }
}
