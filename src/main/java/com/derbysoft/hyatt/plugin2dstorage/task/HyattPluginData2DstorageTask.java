package com.derbysoft.hyatt.plugin2dstorage.task;

import com.derby.dswitch.ari.model.Contract;
import com.derby.dswitch.ari.model.DateSpan;
import com.derby.dswitch.ari.model.RetrieveChangeCriteria;
import com.derby.dswitch.ari.wrap.GetAvailabilityChangeRequest;
import com.derby.nuke.ari.wrap.GetDailyChangeRequest;
import com.derbysoft.schedulecenter.rpc.protocol.Task;
import com.derbysoft.schedulecenter.task.framework.core.TaskExceptionListener;
import com.derbysoft.schedulecenter.task.framework.core.TaskExecuteService;
import com.derbysoft.schedulecenter.task.framework.core.TaskResult;
import com.derbysoft.schedulecenter.task.framework.core.TaskTimer;
import com.google.common.collect.Sets;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service("hyattpluginData2DstorageTask")
public class HyattPluginData2DstorageTask implements TaskExecuteService, TaskExceptionListener {
    private static final Logger LOGGER = LoggerFactory.getLogger(HyattPluginData2DstorageTask.class);

    public static String category_type_dailyInv = "DailyInventory";

    @Autowired
    private BatchTaskFetcher batchTaskFetcher;

    private volatile int step = 2;

    @Override
    public TaskResult execute(Task task) {
        Map<String, String> parametersMap = task.getParametersMap();
        String channelCode = parametersMap.get("channelCode");
        String supplierCode = parametersMap.get("providerCode");
        String hotelCode = parametersMap.get("hotelCode");
        String maxLos = parametersMap.get("maxLos");
        String crNumbers = parametersMap.get("crNumbers");
        List<LocalDate> eachDates = getBy(parametersMap.get("yearMonth"));
        String category = parametersMap.get("category");
        String rateplans = parametersMap.get("rateplans");
        if (StringUtils.isBlank(category)) {
            LOGGER.info("Category is blank");
            return TaskResult.FINISH;
        }

        LOGGER.info("Task:{}", parametersMap.toString());
        if (!"HYATT".equals(supplierCode)) {
            return TaskResult.FINISH;
        }

        Set<Integer> losList = new HashSet<>();
        for (int i = 1; i <= Integer.parseInt(maxLos); i++) {
            if (i > 31) {
                break;
            }
            losList.add(i);
        }

        String[] split = crNumbers.split(",");
        Set<String> crSets = new HashSet<>();
        Collections.addAll(crSets, split);

        if (losList.isEmpty() || crSets.isEmpty()) {
            return TaskResult.FINISH;
        }

        Set<String> ratePlanList = new HashSet<>();
        if (StringUtils.isNotBlank(rateplans)) {
            String[] rpParts = rateplans.split(",");
            Collections.addAll(ratePlanList, rpParts);
        }

        List<GetDailyChangeRequest> dailyChangeRequests = new ArrayList<>();
        if (category.contains(category_type_dailyInv)) {
            for (LocalDate eachDate : eachDates) {
                if (eachDate.isBefore(LocalDate.now())) {
                    continue;
                }

                GetDailyChangeRequest dailyChangeRequest = new GetDailyChangeRequest();
                dailyChangeRequest.setContract(new com.derby.nuke.ari.model.Contract(channelCode, supplierCode));
                com.derby.nuke.ari.model.RetrieveChangeCriteria criteria = new com.derby.nuke.ari.model.RetrieveChangeCriteria();
                criteria.setHotelCode(hotelCode);
                criteria.setType("All");
                criteria.setDateSpan(new com.derby.nuke.ari.model.DateSpan(new LocalDate(eachDate), new LocalDate(eachDate).plusDays(step - 1)));
                criteria.setRequestCategories(Sets.newHashSet(
                        "LosInventory",
                        "Inventory",
                        "Restriction",
                        "Rate"
                ));
                if (!ratePlanList.isEmpty()) {
                    criteria.setRatePlanCodes(ratePlanList);
                }

                dailyChangeRequest.setRetrieveChangeCriteria(criteria);

                dailyChangeRequests.add(dailyChangeRequest);
            }
        }


        List<GetAvailabilityChangeRequest> batchAriRequest = new ArrayList<>();
        //los=30,量太大，一天天抓，
        for (LocalDate eachDate : eachDates) {
            if (eachDate.isBefore(LocalDate.now())) {
                continue;
            }

            GetAvailabilityChangeRequest request = new GetAvailabilityChangeRequest();
            request.setContract(new Contract(channelCode, supplierCode));
            request.setToken("Hyatt-plugin2dstorage");
            RetrieveChangeCriteria criteria = new RetrieveChangeCriteria();
            criteria.setHotelCode(hotelCode);
            criteria.setDateSpan(new DateSpan(new LocalDate(eachDate), new LocalDate(eachDate).plusDays(step - 1)));
            criteria.setLosCandidates(losList);
            criteria.setType("All");
            criteria.setProperty("availability.all", "true");
            criteria.setProperty("los.all", "true");
            criteria.setProperty("los.roomcount", "true");
            criteria.setProperty("categories.all", "false");
            criteria.setProperty("cancel.policy.required", "false");

            if (!ratePlanList.isEmpty()) {
                criteria.setRatePlanCodes(ratePlanList);
            }

            request.setRetrieveChangeCriteria(criteria);

            batchAriRequest.add(request);
        }

        if (!batchAriRequest.isEmpty() || !dailyChangeRequests.isEmpty()) {
            batchTaskFetcher.fetch(channelCode, hotelCode, Integer.parseInt(maxLos), crSets, batchAriRequest, dailyChangeRequests, category);
        }
        return TaskResult.FINISH;
    }

    private List<LocalDate> getBy(String yearMonth) {
        List<LocalDate> dates = new ArrayList<>();
        LocalDate start = new LocalDate(yearMonth + "-01");
        LocalDate end = start.plusMonths(1);
        for (LocalDate date = start; date.isBefore(end); date = date.plusDays(step)) {
            dates.add(date);
        }
        return dates;
    }

    @Override
    public void onException(Class<? extends TaskTimer> aClass, Throwable throwable) {
        LOGGER.error("fail", throwable);
    }

    public void setStep(int step) {
        this.step = step;
    }
}
