package com.derbysoft.hyatt.plugin2dstorage.repository;

import com.derbysoft.ccs.core.MappingCache;
import com.derbysoft.ccs.core.MappingQueryEqualRestriction;
import com.derbysoft.ccs.core.MappingQueryRestriction;
import com.derbysoft.ccs.core.MappingQueryStatusRestriction;
import com.derbysoft.common.repository.ccs.AbstractRepository;
import com.derbysoft.hyatt.plugin2dstorage.domain.Promotion;

import java.util.ArrayList;
import java.util.List;

public class PromotionRepository extends AbstractRepository<Promotion> {

    public PromotionRepository(MappingCache<Promotion> mappingCache) {
        super(mappingCache);
    }

    public List<Promotion> getHotelPromotions(String channelCode, String hotelCode) {
        List<MappingQueryRestriction> restrictions = new ArrayList<>(3);
        restrictions.add(new MappingQueryEqualRestriction("hotelCode", hotelCode));
        restrictions.add(new MappingQueryEqualRestriction("channelCode", channelCode));
        restrictions.add(MappingQueryStatusRestriction.createQueryEnabledStatusRestriction());
        return super.find(restrictions);
    }

}
