package com.derbysoft.hyatt.plugin2dstorage.web;

import com.derby.dswitch.ari.soap.Body;
import com.derby.dswitch.ari.soap.Envelope;
import com.derby.dswitch.ari.soap.Fault;
import com.derby.dswitch.ari.wrap.GetAvailabilityChangeRequest;
import com.derby.dswitch.ari.wrap.GetAvailabilityChangeResponse;
import com.derby.nuke.ari.utils.JibxUtils;
import com.derby.nuke.ari.wrap.GetDailyChangeRequest;
import com.derby.nuke.ari.wrap.GetDailyChangeResponse;
import com.derby.nuke.common.adapter.ari.ARIStreamSerializer;
import com.derby.nuke.common.ws.client.SimpleClient;
import com.derby.nuke.common.ws.client.SimpleClientImpl;
import com.derby.nuke.common.ws.client.http.URLWrapper;
import org.apache.commons.lang3.StringUtils;
import org.jibx.runtime.IBindingFactory;
import org.jibx.runtime.JiBXException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;


@Service
public class PluginWebService {
    private static final Logger LOGGER = LoggerFactory.getLogger(PluginWebService.class);

    private SimpleClient simpleClient = new SimpleClientImpl();
    private ARIStreamSerializer serializer = ARIStreamSerializer.getInstance();

    private IBindingFactory iBindingFactory = JibxUtils.getIBindingFactory();

    private static final Map<String, String> alias = new HashMap<>();

    @Autowired
    private ApsUrlService apsUrlService;


    public PluginWebService() {
        alias.put("DPLATFORM", "GOOGLE");
    }

    public GetAvailabilityChangeResponse getLosRate(GetAvailabilityChangeRequest request) {
        String channelCode = request.getContract().getChannelCode();

        String pluginUrl = apsUrlService.getUrl("HYATT", channelCode);
        String aliaName = alias.get(channelCode);
        if (StringUtils.isNotBlank(aliaName)) {
            request.getContract().setChannelCode(aliaName);
        }

        Envelope reqEnvelope = new Envelope(new Body(request));
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        serializer.marshal(reqEnvelope, byteArrayOutputStream, "UTF-8");

        InputStream inputStream = simpleClient.post(new URLWrapper(pluginUrl + "/origin-retrieve", "GetFromPlugin"), new ByteArrayInputStream(byteArrayOutputStream.toByteArray()));

        Envelope resEnvelope = (Envelope) serializer.unmarshal(inputStream, "UTF-8");
        Body body = resEnvelope.getBody();
        Fault fault = body.getFault();
        if (fault != null) {
            throw new IllegalStateException("GetLosRate fail:" + fault.getCode() + " , " + fault.getString());
        }
        return (GetAvailabilityChangeResponse) body.getMessage();
    }

    public GetDailyChangeResponse getDailyRate(GetDailyChangeRequest request) {
        String channelCode = request.getContract().getChannelCode();

        String pluginUrl = apsUrlService.getUrl("HYATT", channelCode);
        String aliaName = alias.get(channelCode);
        if (StringUtils.isNotBlank(aliaName)) {
            request.getContract().setChannelCode(aliaName);
        }

        try {
            com.derby.nuke.ari.soap.Envelope envelop = new com.derby.nuke.ari.soap.Envelope(new com.derby.nuke.ari.soap.Body(request));
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            iBindingFactory.createMarshallingContext().marshalDocument(envelop, "UTF-8", null, byteArrayOutputStream);

            InputStream inputStream = simpleClient.post(new URLWrapper(pluginUrl + "/original/retrieve.soap", "GetFromPlugin"), new ByteArrayInputStream(byteArrayOutputStream.toByteArray()));

            com.derby.nuke.ari.soap.Envelope response = (com.derby.nuke.ari.soap.Envelope) iBindingFactory.createUnmarshallingContext().unmarshalDocument(inputStream, "UTF-8");
            com.derby.nuke.ari.soap.Body body = response.getBody();
            com.derby.nuke.ari.soap.Fault fault = body.getFault();
            if (fault != null) {
                throw new IllegalStateException("GetDailyRate fail:" + fault.getCode() + " , " + fault.getString());
            }
            return (GetDailyChangeResponse) response.getBody().getMessage();
        } catch (JiBXException e) {
            LOGGER.error("GetDailyRate fail:", e);
            return null;
        }
    }

    public void setApsUrlService(ApsUrlService apsUrlService) {
        this.apsUrlService = apsUrlService;
    }
}
