package com.derbysoft.hyatt.plugin2dstorage.web;

import com.derby.nuke.common.ws.client.SimpleClient;
import com.derby.nuke.common.ws.client.SimpleClientImpl;
import com.derbysoft.warrior.common.json.JsonUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.PreDestroy;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

@Service
public class ApsUrlService {
    private String apsServer = "http://lucy.nuke.derbysoft.com/aps";
    private Map<String, String> cached = new ConcurrentHashMap<>();
    private SimpleClient simpleClient = new SimpleClientImpl();
    private ScheduledFuture<?> scheduledFuture;

    public ApsUrlService() {
        simpleClient.setContentType("application/json");
        scheduledFuture = Executors.newScheduledThreadPool(1).scheduleWithFixedDelay(this::loadAll, 60, 600, TimeUnit.SECONDS);

    }

    public String getUrl(String supplierCode, String channelCode) {
        String pluginUrl = cached.get(channelCode);
        if (StringUtils.isBlank(pluginUrl)) {
            loadAll();
        }
        return cached.get(channelCode); //may exit concurrent, ignore
    }

    private void loadAll() {
        Map<String, Object> params = new HashMap<>();
        params.put("method", "getPositions");
        params.put("params", new String[]{"ari.HYATT."});
        String response = simpleClient.post(apsServer + "/rpc", JsonUtils.toJson(params));

        Map map = JsonUtils.toObject(response, Map.class);
        Object result = map.get("result");
        if (result == null) {
            throw new IllegalStateException("Not found aps value:" + map);
        }

        Map<String, String> values = (Map<String, String>) result;
        cached.clear();
        cached.putAll(values);
        cached.put("MEITUAN","http://hal.derbysoftapi.com/hyattplugin/redirect.ci?Redirect-URL=http://10.0.226.88:8080/hyattplugin");
    }

    @PreDestroy
    public void close() {
        if (scheduledFuture != null) {
            scheduledFuture.cancel(true);
            scheduledFuture = null;
        }
    }


}
