package com.derbysoft.hyatt.plugin2dstorage.executor;

import com.derby.dswitch.ari.model.*;
import com.derby.dswitch.ari.wrap.GetAvailabilityChangeRequest;
import com.derby.dswitch.ari.wrap.GetAvailabilityChangeResponse;
import com.derbysoft.dswitch.core.util.TaskID;
import com.derbysoft.dswitch.dto.common.KeyValue;
import com.derbysoft.dswitch.dto.hotel.cds.*;
import com.derbysoft.dswitch.dto.hotel.common.DateRangeDTO;
import com.derbysoft.dswitch.remote.hotel.buyer.DefaultHotelBuyerRemoteService;
import com.derbysoft.dswitch.remote.hotel.dto.HotelDailyRateChangeRequest;
import com.derbysoft.dswitch.remote.hotel.dto.HotelDailyRateChangeResponse;
import com.derbysoft.dswitch.remote.hotel.dto.RequestHeader;
import com.derbysoft.dswitch.remote.hotel.dto.util.TpaExtensionsUtil;
import com.derbysoft.hyatt.plugin2dstorage.domain.DateRoomCategory;
import com.derbysoft.hyatt.plugin2dstorage.domain.LosAriData;
import com.derbysoft.hyatt.plugin2dstorage.dto.SimpleRQ;
import com.derbysoft.hyatt.plugin2dstorage.translator.LosRateTranslator;
import com.derbysoft.hyatt.plugin2dstorage.web.PluginWebService;
import com.derbysoft.storage.dto.SaveDailyRateRQ;
import com.derbysoft.storage.dto.common.DateRange;
import com.derbysoft.storage.dto.common.OccupancyRates;
import com.derbysoft.storage.dto.common.Rates;
import com.derbysoft.storage.dto.rate.daily.DailyRate;
import com.derbysoft.storage.remote.dto.SaveDailyRateRequest;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.apache.commons.collections.CollectionUtils;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class DailyRateGetter {

    @Autowired
    private PluginWebService pluginWebService;

    @Autowired
    private LosRateTranslator losRateTranslator;

    @Autowired
    private DefaultHotelBuyerRemoteService buyerRemoteService;

    public SaveDailyRateRequest getFromPlugin(SimpleRQ simpleRQ) {
        GetAvailabilityChangeRequest request = new GetAvailabilityChangeRequest();
        request.setContract(new Contract(simpleRQ.getChannelCode(), "HYATT"));
        request.setToken("Hyatt-plugin2dstorage");
        RetrieveChangeCriteria criteria = new RetrieveChangeCriteria();
        criteria.setHotelCode(simpleRQ.getHotelCode());
        criteria.setDateSpan(new DateSpan(new LocalDate(simpleRQ.getDate()), new LocalDate(simpleRQ.getDate())));
        criteria.setLosCandidates(Sets.newHashSet(1));
        criteria.setType("All");
        criteria.setProperty("availability.all", "true");
        criteria.setProperty("los.all", "true");
        criteria.setProperty("los.roomcount", "true");
        criteria.setProperty("categories.all", "false");
        criteria.setProperty("cancel.policy.required", "false");
        request.setRetrieveChangeCriteria(criteria);

        GetAvailabilityChangeResponse response = pluginWebService.getLosRate(request);

        LosAriData losAriData = losRateTranslator.toAriData(simpleRQ.getChannelCode(), simpleRQ.getHotelCode(), 1, simpleRQ.getCrNumbers(), response, () -> new HashSet<>());

        Map<DateRoomCategory, Map<Integer, List<OccupancyRate>>> losRates = losAriData.getLosRates();
        if (losRates.isEmpty()) {
            return null;
        }

        List<DailyRate> dailyRateList = Lists.newArrayList();
        for (Map.Entry<DateRoomCategory, Map<Integer, List<OccupancyRate>>> entry : losRates.entrySet()) {
            DateRoomCategory dateRoomCategory = entry.getKey();
            Map<Integer, List<OccupancyRate>> value = entry.getValue();

            if (value.isEmpty()) {
                continue;
            }
            List<OccupancyRate> occupancyRates = value.get(1);
            if (occupancyRates == null || occupancyRates.isEmpty()) {
                continue;
            }

            if (occupancyRates.size() != 1) {
                throw new IllegalStateException("No dailyRate");
            }

            OccupancyRates warriorOccupancyRates = toWarriorOccRates(occupancyRates.get(0));
            if (warriorOccupancyRates == null) {
                throw new IllegalStateException("invalid dailyRate");
            }

            Rates rates = new Rates();
            rates.setOccupancyRates(warriorOccupancyRates);

            DailyRate dailyRate = new DailyRate();
            dailyRate.setRoomType(dateRoomCategory.getRoomType());
            dailyRate.setRatePlan(dateRoomCategory.getRatePlan());
            dailyRate.setCurrency(occupancyRates.get(0).getCurrencyCode());
            dailyRate.setDateRange(new DateRange(dateRoomCategory.getCheckIn().toString(), dateRoomCategory.getCheckIn().toString()));
            dailyRate.setRates(rates);

            TpaExtensionsUtil.appendElements(dailyRate, new KeyValue("RATE_CHANGE_INDICATOR", "true"));
            dailyRateList.add(dailyRate);

        }

        SaveDailyRateRQ saveDailyRateRQ = new SaveDailyRateRQ();
        saveDailyRateRQ.setChannel(simpleRQ.getChannelCode());
        saveDailyRateRQ.setHotel(simpleRQ.getHotelCode());
        saveDailyRateRQ.setProvider("HYATT");
        saveDailyRateRQ.setDailyRatesList(dailyRateList);

        SaveDailyRateRequest saveDailyRateRequest = new SaveDailyRateRequest();
        saveDailyRateRequest.setHeader(new RequestHeader(simpleRQ.getChannelCode(), "HYATT", TaskID.generate()));
        saveDailyRateRequest.setSaveDailyRateRQ(saveDailyRateRQ);

        return saveDailyRateRequest;

    }

    public SaveDailyRateRequest getFromDstorage(SimpleRQ simpleRQ) {
        HotelDailyRateChangeRequest request = new HotelDailyRateChangeRequest();
        DailyRateChangeRQ dailyRateRQ = new DailyRateChangeRQ();
        dailyRateRQ.setHotelCode(simpleRQ.getHotelCode());
        dailyRateRQ.setDateRange(new DateRangeDTO(simpleRQ.getDate(), simpleRQ.getDate()));
        dailyRateRQ.setTimestamp(String.valueOf(LocalDateTime.now().minusYears(1).toDate().getTime()));
        dailyRateRQ.setType(Type.All);

        request.setHeader(new RequestHeader(simpleRQ.getChannelCode(), "HYATT", TaskID.generate()));
        request.setDailyRateChangeRQ(dailyRateRQ);
        HotelDailyRateChangeResponse response = buyerRemoteService.getDailyRateChange(request);

        List<DailyRateChangeDTO> dailyRateChangeDTOS = Optional.ofNullable(response)
                .map(HotelDailyRateChangeResponse::getDailyRateChangeRS)
                .map(DailyRateChangeRS::getDailyRateChangesList)
                .orElse(null);

        if (CollectionUtils.isEmpty(dailyRateChangeDTOS)) {
            return null;
        }

        List<DailyRate> dailyRateList = Lists.newArrayList();

        for (DailyRateChangeDTO dailyRateChangeDTO : dailyRateChangeDTOS) {
            String roomTypeCode = dailyRateChangeDTO.getRoomTypeCode();
            String ratePlanCode = dailyRateChangeDTO.getRatePlanCode();
            DateRangeDTO dateRange = dailyRateChangeDTO.getDateRange();
            List<OccupancyRateDTO> occupancyRatesList = dailyRateChangeDTO.getOccupancyRatesList();

            Rates rates = new Rates();
            rates.setOccupancyRates(toWarriorOccRates(occupancyRatesList));

            DailyRate dailyRate = new DailyRate();
            dailyRate.setRoomType(roomTypeCode);
            dailyRate.setRatePlan(ratePlanCode);
            dailyRate.setCurrency(dailyRateChangeDTO.getCurrency());
            dailyRate.setDateRange(new DateRange(dateRange.getStart(), dateRange.getEnd()));
            dailyRate.setRates(rates);

            TpaExtensionsUtil.appendElements(dailyRate, new KeyValue("RATE_CHANGE_INDICATOR", "true"));
            dailyRateList.add(dailyRate);
        }

        SaveDailyRateRQ saveDailyRateRQ = new SaveDailyRateRQ();
        saveDailyRateRQ.setChannel(simpleRQ.getChannelCode());
        saveDailyRateRQ.setHotel(simpleRQ.getHotelCode());
        saveDailyRateRQ.setProvider("HYATT");
        saveDailyRateRQ.setDailyRatesList(dailyRateList);

        SaveDailyRateRequest saveDailyRateRequest = new SaveDailyRateRequest();
        saveDailyRateRequest.setHeader(new RequestHeader(simpleRQ.getChannelCode(), "HYATT", TaskID.generate()));
        saveDailyRateRequest.setSaveDailyRateRQ(saveDailyRateRQ);

        return saveDailyRateRequest;
    }

    private OccupancyRates toWarriorOccRates(List<OccupancyRateDTO> occupancyRatesList) {
        List<com.derbysoft.storage.dto.common.OccupancyRate> occRateList = new ArrayList<>();
        for (OccupancyRateDTO occupancyRateDTO : occupancyRatesList) {
            com.derbysoft.storage.dto.common.OccupancyRate warriorRate = new com.derbysoft.storage.dto.common.OccupancyRate();
            warriorRate.setAdult(occupancyRateDTO.getAdult());
            warriorRate.setChild(occupancyRateDTO.getChild());
            warriorRate.setAmountBeforeTax(occupancyRateDTO.getAmountBeforeTax());
            warriorRate.setAmountAfterTax(occupancyRateDTO.getAmountAfterTax());

            occRateList.add(warriorRate);

        }
        OccupancyRates occuRates = new OccupancyRates();
        occuRates.setOccupancyRateList(occRateList);
        return occuRates;
    }


    private OccupancyRates toWarriorOccRates(OccupancyRate occupancyRate) {
        List<com.derbysoft.storage.dto.common.OccupancyRate> occRateList = new ArrayList<>();

        List<Rate> rates = occupancyRate.getRates();
        if (CollectionUtils.isEmpty(rates)) {
            return null;

        }
        for (Rate rate : rates) {
            com.derbysoft.storage.dto.common.OccupancyRate warriorRate = new com.derbysoft.storage.dto.common.OccupancyRate();
            warriorRate.setAdult(rate.getGuestCount());
            warriorRate.setAmountBeforeTax(rate.getAmountBeforeTax().doubleValue());

            occRateList.add(warriorRate);
        }


        OccupancyRates occuRates = new OccupancyRates();
        occuRates.setOccupancyRateList(occRateList);
        return occuRates;
    }

    public void setLosRateTranslator(LosRateTranslator losRateTranslator) {
        this.losRateTranslator = losRateTranslator;
    }

    public void setPluginWebService(PluginWebService pluginWebService) {
        this.pluginWebService = pluginWebService;
    }
}
